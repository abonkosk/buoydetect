package detect;
import java.awt.image.*;
import april.util.*;

public interface ImageSource {
    public boolean start();
    public BufferedImage getImage();
    public double getFrameRate();
    public ParameterGUI getParameterGUI();
}
