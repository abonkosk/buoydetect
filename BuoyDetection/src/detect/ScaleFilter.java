/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package detect;

import Utility.Matrix;
import april.jmat.LinAlg;
import april.util.ParameterGUI;

/**
 *
 * @author jrpeterson
 * 
 * This could potentially be accelerated with a large 2D hashmap that maps from pixel coordinates directly 3D coordinates relative to the camera
 * since this won't change it could be precomputed if this step proves too slow
 * 
 * this still needs more work I'm unsure if the scale math is correct and the constants are probably not quite right
 */

// camera coordinates are x along the width of the image, y down, z out of the front
public class ScaleFilter {
    private double[][] K;           // camera intrinsic matrix
    private double[][] invK;        // inverse of camera intrinsic matrix
    private double[][] correctionR; // corrects for camera not being level with the water
    
    public ScaleFilter() {
        // set up camera intrinsic matrix unless they have a better estimate
        K = new double[][] {{Constants.FOCALLENGTHCOL0, 0, Constants.OPTICALCENTERCOL0},
            {0, Constants.FOCALLENGTHROW0, Constants.OPTICALCENTERROW0},{0, 0, 1}};
        invK = LinAlg.inverse(K);
        correctionR = LinAlg.matrixAB(LinAlg.rotateZ(Constants.ROLLCORRECTION),LinAlg.rotateX(Constants.PITCHCORRECTION));
    }
    
    // if the detected scale at the particular location is consistent then returns true and also modifies the 
    // scale to the more precise estimate here
    public boolean isConsistent(Detection d) {
        // estimate location in 3D
        double[] Pxyzh = projectBuoy(d.cRow,d.cCol);
        
        //System.out.println("buoy at pixel" + d.cCol + ", " + d.cRow);
        //System.out.print("estimated in 3D ");
        //Utility.Matrix.print(Pxyz);
        
        /*double Sfancy = estimateScaleFancy(Pxyz);
        double SPlane = estimateScalePlane(Pxyz);
        double Sdumb = Constants.FOCALLENGTHROW0*Constants.BUOYDIAMETER/(Pxyz[2]);
        System.out.println("Sfancy = "+Sfancy+" SPlane = "+SPlane+" Sdumb = "+ Sdumb);*/
        
        double S;
        // TODO may want to adjust the transition thresholds 
        if (Pxyzh[2] <= 0) {
            return false;                   // if it would map behind the camera, then throw it out
        } else if (Pxyzh[2] <= 2) {
            double[] Pxyz = new double[] {Pxyzh[0], Pxyzh[1], Pxyzh[2]};
            S = estimateScaleFancy(Pxyz);   // if close then we should take the buoy's curvature into account a bit better
        } else if (Pxyzh[2] <= 5) {
            double[] Pxyz = new double[] {Pxyzh[0], Pxyzh[1], Pxyzh[2]};
            S = estimateScalePlane(Pxyz);   // approximates the buoy as a plane
        } else {
            S = Constants.FOCALLENGTHROW0*Constants.BUOYDIAMETER/(Pxyzh[2]); // fast
            // as naive as this is it seems to work suprisingly well even though the x distance is not taken into account
        }
        
        if (Math.abs(S-d.scale) <= Constants.SCALETOLERANCE*S) { // mulitplying by d.scale seemed to work resonably well but less
            // filtering power for things that ought to look really small
            d.scale = S;
            return true;
        } else {
            return false;
        }
    }
    
    // given the pixel center of a buoy estimate its apparent diameter in the image
    private double estimateScale(double row, double col) {
        // first estimate its location in 3D in camera coordinates
        double[] Pxyzh = projectBuoy(row,col);
        
        // chop off the extra component
        double[] Pxyz = new double[] {Pxyzh[0], Pxyzh[1], Pxyzh[2]};
        // compute length of vector
        double D = LinAlg.magnitude(Pxyz);
        
        // compute angle for visual cone
        //double theta = Math.asin(Constants.BUOYDIAMETER/(2*D));
        
        // can skip the arcsin I think since we just need to normalize by distance and apply focal length
        double scale = Constants.FOCALLENGTHROW0*Constants.BUOYDIAMETER/(2*D); // they are close enough together just picked one
        System.out.println("estimated scale of " + scale + " pixels");
        return scale;
    }
    
    /*private double estimateScale(double[] Pxyz, double row, double col) {
        //Matrix.print(Pxyz);
        // compute length of vector
        double D = Matrix.L2norm(Pxyz);
        
        // compute angle for visual cone
        double theta = Math.asin(Constants.BUOYDIAMETER/(2*D));
        
        // use theta to estimate vector in 3D 
        double[] v = new double[] {(Constants.BUOYDIAMETER/2.0)*Math.cos(theta), 0, -(Constants.BUOYDIAMETER/2.0)*Math.sin(theta)};
        //Matrix.print(v);
        // use that vector to create a new point
        double[] Pnewxyz = LinAlg.add(Pxyz,v );
        
        // use camera matrix to project into image
        double[] Pnewimg = LinAlg.matrixAB(K, Pnewxyz);
        
        // normalize homogeneous coordinate
        Pnewimg = LinAlg.scale(Pnewimg, 1.0/Pnewimg[2]);
        
        // now compute scale as the distance between the original point and the new one
        double oldscale = 2*Math.sqrt((Pnewimg[0] - col)*(Pnewimg[0] - col) + (Pnewimg[1] - row)*(Pnewimg[1] - row));
        
        double planescale = estimateScalePlane(Pxyz);
        
        // can skip the arcsin I think since we just need to normalize by distance and apply focal length
        double scale = Constants.FOCALLENGTHROW0*Constants.BUOYDIAMETER/(Pxyz[2]); // they are close enough together just picked one
        
        System.out.println("col = " + col+ " row = "+ row);
        double fancyscale = estimateScaleFancy(Pxyz);
        System.out.println("estimated scale of " + scale + " pixels and other estimate = " + oldscale + " pixels and fancy = " + fancyscale + " plane scale = "+ planescale);
        return fancyscale;
    }*/
    
    // this is likely an overkill way of estimating scale but if this doesn't work I don't know what will
    // uses position in 3D then picks two points which should be on the ball and on the visual cone.  
    // Project those points into the image and then calculate the distance between them
    // its not completely accurate since the detected point isn't actually the centroid
    private double estimateScaleFancy(double[] Pxyz) {
        
        //System.out.println(" got projected point");
        //Matrix.print(Pxyz);
        // compute the length of the position vector
        double Lxyz = LinAlg.magnitude(Pxyz);
        //System.out.println("length = " + Lxyz);
        
        // also consider just the y and z directions to get a length to a horizontal line
        double Lyz = LinAlg.magnitude(new double[] {Pxyz[1],Pxyz[2]});
        //System.out.println("length yz = " + Lyz);
       
        
        // compute angle between the vector to the centroid of the ball and a vector perpendicular 
        // to the ball that passes through the camera
        double theta = Math.asin(Constants.BUOYDIAMETER/(2*Lxyz)); // always positive
        //System.out.println("theta = " + theta);
        
        // compute angle between vector to centroid of the ball and vector to the height and distance
        // of the ball 
        double phi = Math.atan(Pxyz[0]/Lyz);  // may be positive or negative depending on whether buoy is to left
        //System.out.println("phi = " + phi);
        // or right of the optial axis, positive is to the right
        
        // comute points to left and to the right of Pxyz
        double[] leftPxyz = LinAlg.subtract(Pxyz, LinAlg.scale(new double[] {Math.cos(theta - phi), 0, Math.sin(theta - phi)}, Constants.BUOYDIAMETER/2.0));
        double[] rightPxyz = LinAlg.add(Pxyz, LinAlg.scale(new double[] {Math.cos(theta + phi), 0, -Math.sin(theta + phi)}, Constants.BUOYDIAMETER/2.0));
        
        //System.out.print("left point = ");
        //Matrix.print(leftPxyz);
        //System.out.print("Right point = ");
        //Matrix.print(rightPxyz);
        
        // project points
        double[] leftPimg = LinAlg.matrixAB(K,leftPxyz);
        double[] rightPimg = LinAlg.matrixAB(K, rightPxyz);
        
        // normalize image points
        LinAlg.scaleEquals(leftPimg, 1/leftPimg[2]);
        LinAlg.scaleEquals(rightPimg, 1/rightPimg[2]);
        
        //System.out.print("projected left ");
        //Matrix.print(leftPimg);
        //System.out.print("projected right ");
        //Matrix.print(rightPimg);
        
        // compute distance between them and return
        return LinAlg.magnitude(LinAlg.subtract(rightPimg, leftPimg));
    }
    
    // approximates buoy as a plane parallel to image plane
    private double estimateScalePlane(double[] Pxyz) {
        double[] leftPxyz = LinAlg.subtract(Pxyz, new double[] {Constants.BUOYDIAMETER/2.0, 0, 0});
        double[] rightPxyz = LinAlg.add(Pxyz, new double[] {Constants.BUOYDIAMETER/2.0, 0, 0});
        
        //System.out.print("left point = ");
        //Matrix.print(leftPxyz);
        //System.out.print("Right point = ");
        //Matrix.print(rightPxyz);
        
        // project points
        double[] leftPimg = LinAlg.matrixAB(K,leftPxyz);
        double[] rightPimg = LinAlg.matrixAB(K, rightPxyz);
        
        // normalize image points
        LinAlg.scaleEquals(leftPimg, 1/leftPimg[2]);
        LinAlg.scaleEquals(rightPimg, 1/rightPimg[2]);
        
        //System.out.print("projected left ");
        //Matrix.print(leftPimg);
        //System.out.print("projected right ");
        //Matrix.print(rightPimg);
        
        // compute distance between them and return
        return LinAlg.magnitude(LinAlg.subtract(rightPimg, leftPimg));
    }
    
    // uses camera intrinsics and ball characteristics to estimate apparent diameter in pixels
    
    // given the pixel coordinates of the buoy estimates coordinates in 3D relative to camera in camera's coordinate frame
    private double[] projectBuoy(double row, double col) {
        double[] pointH = new double[] {col, row, 1}; // x is along the columns, y down the rows
        
        //System.out.println("got the point at " + col + " " + row);
        
        // compute normalized image coordinate
        double[] pointN = LinAlg.matrixAB(invK, pointH);
        
        //System.out.println("normalized image point = ");
        //Matrix.print(pointN);
        
        
        // convert to homogeneous
        double[] pointNH = new double[] {pointN[0],pointN[1],pointN[2],1};
        
        // correct it so that we are now in a coordinate system level with the water
        double[] pointNC = LinAlg.matrixAB(correctionR, pointNH);
        
        //System.out.println("after rotation correction");
        //Matrix.print(pointNC);
        
        // compute a scale factor, we want to intersect the ray in 3D with the plane of the buoys
        double scaleF = (Constants.CAMERAHEIGHT - Constants.BUOYHEIGHT)/pointNC[1]; // y coordinate is the one that is down
        
        // mutliply 
        return LinAlg.scale(pointNC, scaleF);
    }

    public ParameterGUI getParameterGUI(){
        return null;
    }
}
