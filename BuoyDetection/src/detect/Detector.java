package detect;
import java.awt.image.*;
import april.vis.*;
import april.util.*;

interface Detector {
    public BufferedImage detect(double[][] img);
    public VisObject getVis();
    public ParameterGUI getParameterGUI();
}
