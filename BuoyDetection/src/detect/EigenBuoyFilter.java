/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package detect;

import Utility.ImageProc;
//import Utility.Matrix;
import april.util.ParameterGUI;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import april.jmat.Matrix;
import april.jmat.SingularValueDecomposition;
/**
 *
 * @author jrpeterson
 */

// this filter delivers a response by measuring distance from the window to the space of eigenbuoys
public class EigenBuoyFilter implements Filter {
    
    double[] avgImg;
    Matrix projM;  // projection matrix in space of canonical size to space of eigenbuoys
    ParameterGUI pg;
    
    public EigenBuoyFilter() {
        System.out.println("Beginning EigneBuoy Training");
        // read in all of the training images of buoys
        String dir = "../buoys/trainingdata/";
        // for each category
        ArrayList<double[]> imagesV = new ArrayList<double[]>(Constants.numRED + Constants.numGREEN + Constants.numYELLOW);
        for (int c = 0; c < Constants.numLABELS - 1; c++) {
            System.out.println("Training " + Constants.fileLABELS[c]);
            // for each example
            for (int i = 1; i <= Constants.numExamples[c]; i++) {
                String fileName = dir + Constants.fileLABELS[c] + Integer.toString(i) + ".png";
                BufferedImage img = null;
                try {
                    img = ImageIO.read(new File(fileName));
                } catch (IOException ex) {
                    System.err.println("Error: Could not read a BufferedImage from file '" + fileName + "'");
                    System.exit(-1);
                }
                
                // convert image to grey scale
                double[][] Irgb = ImageProc.imgtoGrey(img);
                
                // scale to the canonical size
                double[][] SIrgb = Utility.Matrix.scale(Irgb, Constants.EIGENCANONICALWIDTH, Constants.EIGENCANONICALHEIGHT);
                
                // add to our arrayList
                imagesV.add(Utility.Matrix.unroll(SIrgb));
            }
        }
        
        // compute the average image
        avgImg = new double[imagesV.get(0).length];
        for (int i = 0; i < imagesV.size(); i++) {
            double[] curImg = imagesV.get(i);
            for (int j = 0; j < curImg.length; j++) {
                avgImg[j] += curImg[j];
            }
        }
        for (int j =0; j < avgImg.length; j++) {
            avgImg[j] /= imagesV.size();
        }
        
        // subtract the average image from everything
        for (int i = 0; i < imagesV.size(); i++) {
            double[] curImg = imagesV.get(i);
            for (int j = 0; j < curImg.length; j++) {
                curImg[j] -= avgImg[j];
            }
        }
        
        // turn the arrayList into a matrix
        double[][] imagesA = Utility.Matrix.compose(imagesV);
        
        System.out.println("imagesA has " + imagesA.length + " rows and " + imagesA[0].length + " columns");
        
        // use singular value decomposition we want the left singular vectors
        // which are the eigenvectors of imagesM*imagesM^T which is exactly the covariance matrix
        Matrix imagesM = new Matrix(imagesA);
        SingularValueDecomposition svd = new SingularValueDecomposition(imagesM.transpose());
        double[][] U = svd.getU().transpose().copyArray(); // left singular vectors transpose
        
        // pull out the number of eigenvectors we care about
        double[][] eigenV = new double[Constants.EIGENCOMPONENTS][U[0].length];
        for (int i = 0; i < Constants.EIGENCOMPONENTS; i++) {
            System.arraycopy(U[i], 0, eigenV[i], 0, U[i].length);
        }
        
        // compose projection
        Matrix uR = new Matrix(eigenV);
        Matrix uC = uR.transpose();
        projM = uC.times(uR);
        
        System.out.println("Projection matrix composed! with " + projM.getRowDimension() + " rows and " + projM.getColumnDimension() + " columns");
        
        // set up parameter gui
        pg = new ParameterGUI();
	//pg.addDoubleSlider("slider", "Sigma", 0.1, 20, sigma);
    }

    @Override
    public int getHeight() {
        return Constants.EIGENCANONICALHEIGHT;
    }

    @Override
    public int getWidth() {
        return Constants.EIGENCANONICALWIDTH;
    }

    @Override
    public ParameterGUI getParameterGUI() {
        return pg;
    }

    @Override
    public double measureResponse(double[][] window) {
        assert window.length == Constants.EIGENCANONICALHEIGHT : "Warning incompatible dimensions";
        assert window[0].length == Constants.EIGENCANONICALWIDTH: "Warning incompatible dimensions";
        
        // unroll the window, subtract the average image and then project onto the space of eigenbuoys
        double[] windowV = Utility.Matrix.subtract(Utility.Matrix.unroll(window),avgImg);
        double[] projV = projM.times(windowV);
        
        // return the squared distance between the two as a measure of how buoy like the image appears
        return Utility.Matrix.L2norm2(Utility.Matrix.subtract(windowV, projV));
    }

    @Override
    public double measureResponse(double[][] img, int startrow, int startcol, int level) {
        double[] windowV = new double[Constants.EIGENCANONICALHEIGHT*Constants.EIGENCANONICALWIDTH];
        
        // going to pull the information out and do the subtraction at the same time
        for (int i = 0; i < Constants.EIGENCANONICALHEIGHT; i++) {
            for (int j = 0; j < Constants.EIGENCANONICALWIDTH; j++) {
                int k = i*Constants.EIGENCANONICALWIDTH + j;
                windowV[k] = img[startrow + i][startcol + j] - avgImg[k];
            }
        }
        
        // project onto space of eigenbuoys
        double[] projV = projM.times(windowV);
        
        // return squared distance
        return Utility.Matrix.L2norm(Utility.Matrix.subtract(windowV,projV));
        
    }

    @Override
    public int getMode() {
        return Constants.MODEPOSITIVE;
    }

    @Override
    public int getMagMode() {
        return Constants.MAGMODELOW;
    }

    @Override
    public void initialize(ImagePyramid imgP) {
        // nothing to do, expect maybe?
    }
    
    @Override
    public double[][] getFilter() {
        return Utility.Matrix.roll(avgImg, Constants.EIGENCANONICALWIDTH);
    }
    
    @Override
    public double[] getThresholds() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
