package detect;

import april.util.*;

public interface Filter {

    /** returns the size of the square window that
	should be used for this filter **/
    // also note that this tells us the native scale of the 
    public int getHeight();
    
    public int getWidth();

    /** returns this filter's parameter GUI for
	dynamic parameter tweaking! **/
    public ParameterGUI getParameterGUI();

    /** returns a measure of the Buoyness of the window **/
    public double measureResponse(double[][] window);
    
    // where start row and start col is the upper left hand corner of the window to be measured
    public double measureResponse(double[][] img, int startrow, int startcol, int level);
    
    // is a larger value for response more buoy like or is a lower value
    // if response is an error from buoy then this will return false
    // do we want positive or negative values or both
    public int getMode();
    
    // do we want large magnitudes or small ones
    public int getMagMode();
    
    public void initialize(ImagePyramid imgP);
    
    public double[][] getFilter();
    
    public double[] getThresholds();

}
