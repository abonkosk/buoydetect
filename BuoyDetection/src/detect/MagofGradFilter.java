/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package detect;

import Utility.ImageProc;
import Utility.Matrix;
import april.jmat.LinAlg;
import april.util.ParameterGUI;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author jrpeterson
 */
public class MagofGradFilter implements Filter {
    ImagePyramid gradPyramid;
    private double[][] filtertemplate;
    ParameterGUI pg;
    
    // construct gradient magnitude
    double[][] sobelX = new double[][] {{-1, 0, 1}, {-2, 0, 2},{-1, 0, 1}};
    double[][] sobelY = new double[][] {{-1, -2, -1},{0, 0, 0},{1, 2, 1}};
    boolean imageInit;
    
    public MagofGradFilter() {
        // create filter template as average gradient
        System.out.println("Begining Gradient Magnitude training");
        String dir = "../buoys/trainingdata/";
        // for each category red, green, blue, water
        filtertemplate = new double[Constants.GRADCANONICALHEIGHT][Constants.GRADCANONICALWIDTH];
        int total = 0;
        for (int c = 0; c < Constants.numLABELS - 1; c++) {
            // read in each training example
            System.out.println("Training " + Constants.fileLABELS[c]);
            for (int i = 1; i <= Constants.numExamples[c]; i++) {
                String fileName = dir + Constants.fileLABELS[c] + Integer.toString(i) + ".png";
                // acquire and set image
                BufferedImage img = null;
                try {
                    img = ImageIO.read(new File(fileName));
                } catch (IOException ex) {
                    System.err.println("Error: Could not read a BufferedImage from file '" + fileName + "'");
                    System.exit(-1);
                }
                
                // convert to gray scale
                double[][] ImgGray = ImageProc.imgtoGrey(img);
                
                // compute gradients
                double[][] Gx = Matrix.convolveCBounded(ImgGray, sobelX);
                double[][] Gy = Matrix.convolveCBounded(ImgGray, sobelY);
                
                // compute magnitude of gradient
                double[][] magG = new double[Gx.length][Gx[0].length];
                for (int k = 0; k < magG.length; k++) {
                    for (int j = 0; j < magG[0].length; j++) {
                        magG[k][j] = Math.sqrt(Gx[k][j] * Gx[k][j] + Gy[k][j] * Gy[k][j]);
                    }
                }
                
                // rescale to the appropriate size
                double[][] magGS = Matrix.scale(magG, Constants.GRADCANONICALWIDTH, Constants.GRADCANONICALHEIGHT);
                LinAlg.plusEquals(filtertemplate,magGS); // accumulate
                total++;
            }
        }
        LinAlg.scaleEquals(filtertemplate, 1.0/total);
        imageInit = false;
        pg = new ParameterGUI();
        pg.addDoubleSlider("slider", "MagOfGrad Sigma", 0.1, 20, 10);
        System.out.println("Training Complete");
        
    }
    
    @Override
    public void initialize(ImagePyramid imgPyramid) {
        
        gradPyramid = new ImagePyramid(imgPyramid.levels.size());
        for (ImageSample l: imgPyramid.levels) {
            double[][] Gx = Matrix.convolveC(l.data, sobelX);
            double[][] Gy = Matrix.convolveC(l.data, sobelY);
            
            double[][] magG = new double[Gx.length][Gx[0].length];
            for (int i = 0; i < magG.length; i++) {
                for (int j =0; j < magG[0].length; j++) {
                    magG[i][j] = Math.sqrt(Gx[i][j]*Gx[i][j] + Gy[i][j]*Gy[i][j]);
                }
            }
            
            gradPyramid.levels.add(new ImageSample(magG,l.downsample));
        }
        imageInit = true;
    }

    @Override
    public int getHeight() {
        return filtertemplate.length;
    }

    @Override
    public int getWidth() {
        return filtertemplate[0].length;
    }

    @Override
    public ParameterGUI getParameterGUI() {
        return pg;
    }

    // this is not the way we want to do things
    
    public double measureResponse(int level, int startrow, int startcol) {
        assert imageInit : "Warning uninitialized";
        // grab the response for that offset field to maintain compatability with everything
        // could tecnically just hand over this image pyramid but then that would change how image coordinates are interpreted
        double[][] data = gradPyramid.levels.get(level).data;
        //System.out.println("MAG: level rows = " + data.length + " columns = "+ data[0].length);
        //return data[startrow+Constants.GRADCANONICALHEIGHT/2][startcol+Constants.GRADCANONICALWIDTH]; // this will simply return the magnitude of the gradient
        // convolution mode, sort of works but is honestly not that great
        if (!Constants.MAGOFGRADTEMPLATEMODE) {
            return Matrix.resolveSub(data, startrow, startcol,filtertemplate);
        }
        // template mode, do template matching of a window with the filter
        else {
            double Error = 0; // squared template error
            for (int i = 0; i < filtertemplate.length; i++) {
                for (int j = 0; j < filtertemplate[0].length; j++) {
                    Error += (data[startrow+i][startcol+j] - filtertemplate[i][j])*(data[startrow+i][startcol+j] - filtertemplate[i][j]);
                }
            }
            return Error;
        }
    }

    @Override
    public int getMode() {
        return Constants.MODEPOSITIVE;
    }
    
    @Override
    public int getMagMode() {
        if (!Constants.MAGOFGRADTEMPLATEMODE) {
            return Constants.MAGMODEHIGH;
        } else {
            return Constants.MAGMODELOW;
        }
    }

    @Override
    public double measureResponse(double[][] window) {
        // the method that this function uses breaks the object orientedness since the gradients are precomputed
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public double measureResponse(double[][] img, int startrow, int startcol, int level) {
        // the method that this function uses breaks the object orientedness since the gradients are precomputed
        return measureResponse(level, startrow, startcol);
    }

    @Override
    public double[][] getFilter() {
        return filtertemplate;
    }
    
    @Override
    public double[] getThresholds() {
        return new double[] {Constants.MINRESPONSETHRESHOLD_MAGOFGRAD};
    }
}
