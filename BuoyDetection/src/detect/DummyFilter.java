package detect;

import april.util.*;

public class DummyFilter implements Filter {
    private final int window_size = 100;
    ParameterGUI pg;
    
    // dumb stuff
    double val = 0.0;
    double inc = 0.15;
    double max = 1.0;

    public DummyFilter(){
	pg = new ParameterGUI();
	pg.addDoubleSlider("slider", "Slider", 0.0, 1.0, 0.5);
    }

    @Override
    public int getWidth(){
	return window_size;
    }
    
    public int getHeight() {
        return window_size;
    }

    @Override
    public ParameterGUI getParameterGUI(){
	return pg;
    }

    @Override
    public double measureResponse(double[][] window){
	assert(window != null &&
	       window.length == window_size &&
	       window[0].length == window_size);

	double ret = val;
	val += inc;
	while(val > max)
	    val -= max;

	return ret;
    }
    
    @Override 
public int getMode() {
        return 1;
    }

    @Override
    public double measureResponse(double[][] img, int startrow, int startcol, int level) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getMagMode() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void initialize(ImagePyramid imgP) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public double[][] getFilter() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    @Override
    public double[] getThresholds() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
