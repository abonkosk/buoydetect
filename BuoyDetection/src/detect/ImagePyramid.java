package detect;

import Utility.ImageProc;
import java.util.*;
import java.awt.image.*;
import Utility.Matrix;

public class ImagePyramid {
    
    public ArrayList<ImageSample> levels; // maintains a list of sampled versions of the image
    
    public ImagePyramid(int numLevels) {
        levels = new ArrayList<ImageSample>(numLevels);
    }
    
    // for now just downsamples the image at a rate of 2
    // and the numLevels is how many times it does this downsampling - 1
    // so it is exacty equal to the number of levels that are created
    // TODO at the moment taking the mean of each pixel
    public ImagePyramid(BufferedImage img, int numLevels) {
        assert(img != null) : "Expected non null image!";
        assert(numLevels >= 1) : "Must create atleast a single Level";
        
        assert (img.getWidth()*Math.pow(0.5, numLevels) >= 1) : "Warning tried to downsample too much";
        assert (img.getHeight()*Math.pow(0.5, numLevels) >= 1) : "Warning tried to downsample too much";
        
        initialize(ImageProc.imgtoGrey(img),numLevels);
    }
    
    public ImagePyramid(BufferedImage img) {
        assert(img != null) : "Expected non null image!";
        initializeVarScale(ImageProc.imgtoGrey(img));
    }
    
    public ImagePyramid(double[][] img, int numLevels) {
        assert(img != null);
        assert(numLevels >= 1) : "Must create atleast a single Level";
        
        assert (img[0].length*Math.pow(0.5, numLevels) >= 1) : "Warning tried to downsample too much";
        assert (img.length*Math.pow(0.5, numLevels) >= 1) : "Warning tried to downsample too much";
        
        initialize(img, numLevels);
    }
    
    private void initialize(double[][] img, int numLevels) {
        levels = new ArrayList<ImageSample>(numLevels);
        levels.add(new ImageSample(img,1));
        
        // this convolution makes the edges appear darker but does not disrupt pixel coordinates
        double[][] G = Matrix.Gaussian2D(1.5, 3); // 3 is the smallest that can be made 
        // need to pick sigma high enough to avoid aliasing
        
        for (int l = 1; l < numLevels; l++) {
            double[][] previous = levels.get(l-1).data;
            //double[][] current = Matrix.convolveC(previous, G); // blur
            
            // downsample
            double[][] currentD = new double[previous.length/2][previous[0].length/2];
            for (int y = 0; y < currentD.length; y++) {
                for (int x = 0; x < currentD[0].length; x++) {
                    currentD[y][x] = Matrix.convolveCPoint(previous, G, y*2, x*2); //current[y*2][x*2]; // since we rounded down above don't think it can go out of bounds
                }
            }
            
            levels.add(new ImageSample(currentD,(levels.get(l-1).downsample)*2));
        }
    }
    
    // uses a more flexible ratio between scales to obtain image pyramids with greater scalar resolution
    private void initializeVarScale(double[][] img) {
        levels = new ArrayList<ImageSample>(Constants.IMGP_NUMLEVELS);
        levels.add(new ImageSample(img,1));
        
        double[][] G = Matrix.Gaussian2D(1.0, 3);
        
        for (int l = 1; l < Constants.IMGP_NUMLEVELS; l++) {
            double[][] previous = levels.get(l-1).data;
            double ratio = Constants.IMGP_SCALERATIO[l - 1];
            
            double[][] currentD = new double[(int) Math.floor(previous.length/ratio)][(int) Math.floor(previous[0].length/ratio)];
            
            /*for (int y = 0; y < currentD.length; y++) {
                for (int x = 0; x < currentD[0].length; x++) {
                    currentD[y][x] = Matrix.convolveCPoint(previous, G, (int) Math.floor(y*ratio), (int) Math.floor(x*ratio)); //current[y*2][x*2]; // since we rounded down above don't think it can go out of bounds
                }
            }*/
            // lets try inlining the convolution and removing bounds checking by not trying to set edge points
            double[] prevRow, GRow;
            for (int y = 1; y < currentD.length - 1 ; y++) {
                int cury = (int) Math.floor(y*ratio);
                for (int x = 1; x < currentD[0].length - 1 ; x++) {
                    double R = 0;
                    int curx = (int) Math.floor(x*ratio);
                    for (int j = -1; j <= +1; j++) {
                        prevRow = previous[cury+j];
                        GRow = G[j+1];
                        for (int i = -1; i <= +1; i++) {
                            //R += previous[cury+j][curx+i]*G[j+1][i+1]; // symmetric so we can save on reversal math too
                            R += prevRow[curx+i]*GRow[i+1];
                        }
                    }
                    currentD[y][x] = R;
                }
            }
            
            levels.add(new ImageSample(currentD,(levels.get(l-1).downsample)*ratio));
        }
    }
    
    public void printDetails() {
        for (ImageSample s: levels) {
            s.printDetails();
        }
    }
    
    public ArrayList<ImageSample> getPyramid() {
        return levels;
    }
    
    public ImageSample getLevel(int i) {
        return levels.get(i);
    }
    
    public ImagePyramid copyStructure() {
        ImagePyramid newP = new ImagePyramid(this.levels.size());
        
        // initialzie each level with matrix of zeros
        for (int i = 0; i < this.levels.size(); i++) {
            newP.levels.add(this.levels.get(i).CreateBassedOn());
        }
        return newP;
    }
    
    

    /*
    public static ArrayList<ImageSample> getPyramid(BufferedImage img){
	assert(img != null);
	byte data[] = ((DataBufferByte)(img.getRaster().getDataBuffer())).getData();

	System.out.println("length="+data.length+" height="+img.getHeight()+" width="+img.getWidth());

	ArrayList<ImageSample> ret = new ArrayList<ImageSample>();

        ImageSample full = new ImageSample(new double[img.getHeight()][img.getWidth()],1);
        
	for(int i = 0; i < data.length; i += 3)
	    full.data[i/3] = computeIntensity(data[i], data[i+1], data[i+2]);

	ret.add(full);
	return ret;
    }*/

    static double computeIntensityP(byte red, byte green, byte blue){
	return 0.2989*(double)((int)(red)&0xff) +
	    0.5870*(double)((int)(green)&0xff) +
	    0.1140*(double)((int)(blue)&0xff);
    }
    
    static double computeIntensity(byte red, byte green, byte blue) {
        double value = (double) ((int)red & 0xff);
        value += (double) ((int)green & 0xff);
        value += (double) ((int)blue & 0xff);
        value /= 3.0;
        return value;
    }
    
    
}
