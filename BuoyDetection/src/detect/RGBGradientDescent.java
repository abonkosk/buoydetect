/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package detect;

import Utility.ImageProc;
import april.jmat.LinAlg;
import java.awt.image.BufferedImage;

/**
 *
 * @author jrpeterson
 * 
 * operates in normalized rgb rather than straight rgb
 */
public class RGBGradientDescent {
    // given an initial guess uses graident descent to try and minimize the magnitude of the error betwee
    // the average color in the window and the average color 
    public static Detection descend(BufferedImage Img, Detection init, double[] rgbTarget) {
        double scale = init.scale;
        int epsilon = (int) Math.round(scale*0.01); // will try and step 20% of scale
        if (epsilon < 1) {
            epsilon = 1;
        }
        
        System.out.println("got a detection of type " + Constants.fileLABELS[init.type]);
        double steplength = scale*0.05;
        int[] curX = new int[] {init.cRow, init.cCol};
        double lastE = Double.MAX_VALUE;
        double curE = Double.MAX_VALUE;
        int[] deltaX;
        int iter = 0;
        do {
            // compute gradient in image space
            double[] currgb = ImageProc.extractRGB(Img, curX[0], curX[1], scale);
            //LinAlg.normalizeEquals(currgb);
            //System.out.print("Current RGB = ");
            Utility.Matrix.print(currgb);
            double[] rowrgb = ImageProc.extractRGB(Img, curX[0] + epsilon, curX[1], scale);
            //LinAlg.normalizeEquals(rowrgb);
            double[] colrgb = ImageProc.extractRGB(Img, curX[0], curX[1] + epsilon, scale);
            //LinAlg.normalizeEquals(colrgb);
            
            // compute error
            lastE = curE; // save away the old one
            curE = LinAlg.magnitude(LinAlg.subtract(rgbTarget, currgb));
            double rowE = LinAlg.magnitude(LinAlg.subtract(rgbTarget, rowrgb));
            double colE = LinAlg.magnitude(LinAlg.subtract(rgbTarget, colrgb));
            
            // compute change in errors
            double dEdrow = rowE - curE;
            double dEdcol = colE - curE;
            
            // compute gradient direction to go in
            double[] dir = LinAlg.normalize(new double[] {dEdrow, dEdcol});
            
            // compute step we want to go opposite the gradient
            deltaX = new int[] {(int) -Math.round(dir[0]*steplength), (int) -Math.round(dir[1]*steplength)};
            
            // update
            curX[0] += deltaX[0];
            curX[1] += deltaX[1];

	    // did we fall off the edge of the image
	    // Note to John: Bounds checking is a really good practice!
	    if(curX[0] < 0 || curX[1] < 0)
	      break;

            iter++;

            // termination condition
        } while ((lastE - curE > Constants.GRADDESCENTERRORTHRESHOLD) && (iter < Constants.GRADDESCENTMAXITERATIONS));
        
        
        if (iter < Constants.GRADDESCENTMAXITERATIONS) {
            // undo the last deltaX since it wasn't favorable
            curX[0] -= deltaX[0];
            curX[1] -= deltaX[1];
        }
        
        System.out.println(" took " + (iter - 1) + " steps");
        
        // create a new detection
        Detection newD =  init.copy();
	newD.cRow = curX[0];
	newD.cCol = curX[1];

	//System.out.println("Grad_descent="+newD.Response+" init.cCol="+init.cCol+" init.cRow="+init.cRow+" cCol="+newD.cCol+" cRow="+newD.cRow+" iter="+iter);
        return newD;
    }
}
