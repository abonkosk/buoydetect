package detect;
import java.awt.image.*;
import java.io.*;
import april.util.*;
import lcm.lcm.*;
import lcmtypes.*;

public class LCMImageSource implements ImageSource, LCMSubscriber {
    LCM lcm;
    String channel;
    image_t last_msg = null;
    BufferedImage last_img = null;

    // frame rate tracking
    long last_time = -1;
    double last_rate = -1;

    Object lock = new Object();

    public LCMImageSource(String channel){
	lcm = LCM.getSingleton();
	this.channel = channel;
    }

    public boolean start(){
	lcm.subscribe(channel, this);
	return true;
    }

    public BufferedImage getImage(){
	synchronized(lock){
	    if(last_msg == null)
		return null;

	    // convert LCM image_t to a BufferedImage
	    if(last_img == null){
		last_img = new BufferedImage(last_msg.width, last_msg.height, BufferedImage.TYPE_3BYTE_BGR);
		byte[] img_data = ((DataBufferByte)(last_img.getRaster().getDataBuffer())).getData();
		for(int j = 0; j < last_msg.size; ++j)
		    img_data[j] = last_msg.data[j];
	    }

	    if(last_time != -1){
		last_rate = (double)1000000000/(double)(System.nanoTime() - last_time);
	    }
	    last_time = System.nanoTime();

	    return last_img;
	}
    }

    public double getFrameRate(){
	return last_rate;
    }

    public ParameterGUI getParameterGUI(){
	return null;
    }

    public void messageReceived(LCM lcm, String channel, LCMDataInputStream dins) {
	image_t msg;
	try {
	    msg = new image_t(dins);
	} catch(Exception ex){
	    System.err.println("LCM Error!");
	    return;
	}

	synchronized(lock){
	    last_msg = msg;
	    last_img = null;
	}
    }

}
