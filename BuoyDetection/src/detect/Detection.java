/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package detect;
import java.awt.image.*;

/**
 *
 * @author jrpeterson
 */
public class Detection {
    public int cRow;
    public int cCol;
    public double Response;
    public double scale;
    public int type; // what is it?
    public double dist;
    public BufferedImage img;

    public Detection(int cRow, int cCol, double Response, double scale) {
        this.cRow = cRow;
        this.cCol = cCol;
        this.Response = Response;
        this.scale = scale;
        this.type = Constants.UNKNOWN;
	this.dist = -1;
	this.img = null;
    }

	
    public Detection copy() {
        Detection newd = new Detection(cRow,cCol,Response,scale);
        newd.type = this.type;
        newd.img = this.img;
        newd.dist = this.dist;
        return newd;
    }

}
