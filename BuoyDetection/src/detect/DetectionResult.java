package detect;

import Utility.Suppression;
import Utility.Matrix;
import Utility.ImageProc;
import Utility.Line;
import Utility.StopWatch;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.*;

public class DetectionResult {
    Filter filter;
    public ImagePyramid imgP = null;
    public ShoreDetector shoreD = null;
    public ImagePyramid responseP = null;
    public int[] offset = null;
    public ArrayList<Double> scales = null;
    public ArrayList<ArrayList<Detection>> multiScaleRPeaks = null;
    public ArrayList<Detection> scaleConsistent = null;
    public ArrayList<Detection> resolvedDetections = null;
    public int[] numberD = null;
    public ArrayList<Detection> buoys = null;
    public ArrayList<Detection> water = null;
    public ArrayList<Detection> descended = null;
    public ArrayList<Detection> nearBuoys = null;
    public ArrayList<Detection> filtered = null;
    public ArrayList<Detection> filteredNearBuoys = null;
    public StopWatch sw = null;

    DetectionResult(Filter filter){
	this.filter = filter;
    }

    public void displayResponses(DrawThread draw){
        //display responses
	for (int i = 0; i < responseP.levels.size(); i++) {
            System.out.println("Response pyramid level " + i + " " + responseP.levels.get(i).data.length + " x " + responseP.levels.get(i).data[0].length);
            if (filter.getMagMode() == Constants.MAGMODELOW) {
                draw.setImage(Utility.Matrix.visualize(ImageProc.invert(responseP.levels.get(i).data),1));
            } else {
                draw.setImage(Utility.Matrix.visualize(responseP.levels.get(i).data,1));
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(BuoyDetect.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        draw.setImage(Utility.Matrix.visualize(responseP.levels.get(0).data,1));
    }

    public void displayShoreGrad(DrawThread draw){
        // display the gradient image that it acquired
        draw.setImage(Matrix.visualize(shoreD.getGrad(), 1));
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            Logger.getLogger(BuoyDetect.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void displayFilter(DrawThread draw){
        //display the filter we are using
        draw.setImage(Matrix.visualize(filter.getFilter(), 1));
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            Logger.getLogger(BuoyDetect.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void displayImagePyramid(DrawThread draw){
        // display downsampled images
        for (int i = 0; i < imgP.levels.size(); i++) {
            System.out.println("Image pyramid level " + i + " " + imgP.levels.get(i).data.length + " x " + imgP.levels.get(i).data[0].length);
            draw.setImage(Matrix.visualize(imgP.levels.get(i).data,1));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(BuoyDetect.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public ArrayList<Detection> getDetections(){
	if(filteredNearBuoys != null)
	     return filteredNearBuoys;
	if(filtered != null)
	     return filtered;
	if(nearBuoys != null)
	    return nearBuoys;
	if(descended != null)
	    return descended;
	if(resolvedDetections != null)
	    return resolvedDetections;
	return scaleConsistent;
    }

    public ArrayList<Detection> getAllDetections(){
	ArrayList<Detection> buoyDet = getDetections();
	ArrayList<Detection> ret = new ArrayList<Detection>();
	if(buoyDet != null)
	    ret.addAll(buoyDet);
	if(water != null)
	    ret.addAll(water);
	return ret;
    }

    public void filterDetections(double limit){
	if(resolvedDetections != null){
	    filtered = new ArrayList<Detection>();
	    for(Detection d : resolvedDetections){
		if(d.dist <= limit)
		    filtered.add(d);
	    }
	}

	if(nearBuoys != null){
	    filteredNearBuoys = new ArrayList<Detection>();
	    for(Detection d : nearBuoys){
		if(d.dist < limit)
		    filteredNearBuoys.add(d);
	    }
	}
    }

}
