/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package detect;

import java.awt.image.BufferedImage;
import april.util.ParameterGUI;

/**
 *
 * @author jrpeterson
 */
public interface Classifier {
    
    public int classify(BufferedImage img, Detection d);    
    public double[] getNearest(BufferedImage img, Detection d);
    public ParameterGUI getParameterGUI();
}
