package detect;
import java.awt.image.BufferedImage;
import java.io.*;
import javax.imageio.*;
import Utility.ImageProc;
import april.util.*;

public class DetectorTest implements ParameterListener {    
    Detector d = new ShoreDetector();
    DrawThread draw;
    String directory;
    File[] filenames;
    int current_file = 0;
    ParameterGUI pg = new ParameterGUI();
    boolean reread_image = false;

    public static void main(String[] args){
	if(args.length < 1){
	    System.err.println("Error: No directory provided!");
	    System.exit(-1);
	}

	new DetectorTest(args[0]).start();
    }

    public DetectorTest(String directory){
	this.directory = directory;
	draw = new DrawThread("Shore Detector");
	draw.start();
	
	pg.addButtons("next", ">");
	pg.addListener(this);
    }

    public void parameterChanged(ParameterGUI pg, String name){
	if(name.equals("next")){
	    current_file++;
	    current_file %= filenames.length;
	    reread_image = true;
	}
    }

    public void start(){
	// acquire and set image

	File dir = new File(directory);
	filenames = dir.listFiles();
	if(filenames == null || filenames.length == 0){
	    System.err.println("Error: Invalid directory or directory is empty");
	    System.exit(-1);
	}

	draw.addParameterGUI(d.getParameterGUI());
	draw.setVerticalParameterGUI(pg);

	
	while(true){

	    File f = filenames[current_file];
	    
	    BufferedImage img = null;
	    try {
		img = ImageIO.read(f);
	    } catch(IOException ex) {
		System.err.println("Error: Could not read a BufferedImage from file '"+f.getName()+"'");
		System.exit(-1);
	    }

	    if(img == null){
		System.out.println("img is null!");
		continue;
	    }

	    while(!reread_image){

		double[][] imgA = ImageProc.imgtoGrey(img);
		BufferedImage res = d.detect(imgA);
		if(res != null)
		    draw.setImage(res);
		else
		    draw.setImage(img);
	    
		draw.setVisObject(d.getVis());
	    }

	    reread_image = false;
	}


    }
}
