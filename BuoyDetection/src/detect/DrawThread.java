package detect;

// java libraries
import Utility.Line;
import Utility.Matrix;
import Utility.StopWatch;
import april.jmat.LinAlg;
import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;

// april libraries
import april.vis.*;
import april.util.*;
import april.jmat.geom.*;
import java.util.ArrayList;

public class DrawThread extends Thread {
    private final double UPDATE_FREQ = 10; //10 Hz
    JFrame jf;
    JPanel pg_panel;
    VisWorld vw;
    VisLayer vl;
    VisCanvas vc;
    boolean first_draw = true;
    Region currentRegion = null;

    Object lock = new Object();

    BufferedImage last_img;
    VisObject last_vo = null;
    StopWatch last_sw = null;
    double frame_rate = -1;

    MouseEventHandler eh;
    int last_x = -1, last_y = -1;
    // row, column, response, scale
    ArrayList<Detection> detections; // array which describes detections
    int image_height = -1;
    int image_width = -1;
    Line shore;

    // setVisible delay state... BIG BIG BIG HACK!!!!
    boolean inFirstVisible = true;
    long initTime = -1;
    final long WAIT_TIME = 0*1000000;

    public DrawThread(String window_name){
	jf = new JFrame(window_name);
	vw = new VisWorld();
	vl = new VisLayer(vw);
	vc = new VisCanvas(vl);

	eh = new MouseEventHandler();
	vl.addEventHandler(eh);

        pg_panel = new JPanel();
	pg_panel.setLayout(new BoxLayout(pg_panel, BoxLayout.Y_AXIS));
	
	jf.setLayout(new BorderLayout());
	jf.add(vc, BorderLayout.CENTER);
	jf.add(pg_panel, BorderLayout.SOUTH);

        jf.setSize(1450,1000);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	makeVisible(true);
    }

    // A hack to delay displaying window on intialization
    void makeVisible(boolean required){
	if(!inFirstVisible){
	    if(required)
		jf.setVisible(true);
	}

	else {

	    if(initTime == -1){
		initTime = System.nanoTime();
		return;
	    }
	    
	    if(System.nanoTime() - initTime > WAIT_TIME){
		inFirstVisible = false;
		jf.setVisible(true);
	    }

	}
    }

    public Region getSelectedRegion(){
	return currentRegion;
    }

    public void clearSelectedRegion(){
	synchronized(lock){
	    currentRegion = null;
	}
    }

    public void enableBuffer(String buffer, boolean v){
	vl.setBufferEnabled(buffer, v);
    }

    public void addParameterGUI(ParameterGUI pg){
        if(pg != null){
          pg_panel.add(pg);
	  makeVisible(true);
	}
    }

    public void setVerticalParameterGUI(ParameterGUI pg){
	jf.add(pg, BorderLayout.WEST);
	makeVisible(true);
    }

    public void clearAll(){
	synchronized(lock){
	    last_img = null;
	    first_draw = true;
	    last_vo = null;
	    detections = null;
	    shore = null;
	    last_x = -1;
	    last_y = -1;
	}
    }

    public void setFrameRate(double frame_rate){
	synchronized(lock){
	    this.frame_rate = frame_rate;
	}
    }

    public void setStopWatch(StopWatch sw){
	synchronized(lock){
	    last_sw = sw;
	}
    }

    public void setImage(BufferedImage img){
	synchronized(lock){
	    last_img = img;
            //first_draw = true; // rescale when we change images
	}
    }

    public void setVisObject(VisObject vo){
	synchronized(lock){
	    last_vo = vo;
	}
    }

    public void setDetections(ArrayList<Detection> detections) {
        synchronized(lock) {
            this.detections = new ArrayList<Detection>(detections.size());
            for (Detection d : detections) {
                this.detections.add(d.copy());
            }
	    //last_y = -1;
	    //last_x = -1;
        }
    }
    
    public void setLine(Line l) {
        this.shore = l;
    }

    public void run(){

	while(true){

	    makeVisible(false);

	    try {
		Thread.sleep((long)(1000/UPDATE_FREQ));
	    } catch(InterruptedException ex){}

	    synchronized(lock){

		{
		    VisWorld.Buffer vb = vw.getBuffer("image");
		    if(last_img != null){
			vb.addBack(new VzImage(new VisTexture(last_img, VisTexture.NO_MIN_FILTER), VzImage.FLIP));
		    }
		    vb.swap();
		    
		    if(last_img != null && first_draw){
			first_draw = false;
			vl.cameraManager.fit2D(new double[] { 0, 0 },
					       new double[] { last_img.getWidth(), last_img.getHeight() },
					       true);
			image_height = last_img.getHeight();
			image_width = last_img.getWidth();
		    }

		}
                


		{
                    ArrayList<VisWorld.Buffer> vbs = new ArrayList<VisWorld.Buffer>();
		    VisWorld.Buffer vb_train = vw.getBuffer("training");
                    
                    for (int i = 0; i < Constants.numLABELS; i++) {
                        vbs.add(vw.getBuffer(Constants.fileLABELS[i]));
                    }
                    

		    if (detections != null) {                    
			
			Detection best_det = null;
			double best_dist = -1;
			for (Detection d: detections) {
			    // TODO relate color to detection strength scale between threshold and strongest observed
			    //System.out.println("drawing at " + d[1] + ", "+ -d[0]+ " with scale = "+ d[3]);
			    double dx = (double)d.cCol - (double)last_x;
			    double dy = (double)(image_height - d.cRow) - (double)last_y;
			    double dist = Math.sqrt(dx*dx + dy*dy);
			    if(dist < best_dist || best_dist == -1){
				best_det = d;
				best_dist = dist;
			    }

			    Color c;
			    if (d.type == Constants.RED) {
				c = Color.RED;
			    } else if (d.type == Constants.GREEN) {
				c = Color.GREEN;
			    } else if (d.type == Constants.YELLOW) {
				c = Color.YELLOW;
			    } else if (d.type == Constants.WATER) {
				c = Color.BLUE;
			    } else {
				c = Color.BLACK;
			    }
			    VisChain ball = new VisChain(LinAlg.translate(d.cCol,image_height - d.cRow, 2.0),
							 new VzCircle(d.scale/2.0,new VzLines.Style(c, 2)));
			    vbs.get(d.type).addBack(ball);
			}

			if(last_x != -1 && last_y != -1 && best_det != null){
			    vbs.get(best_det.type).
				addBack(new VisChain(LinAlg.translate(best_det.cCol,
								      image_height - best_det.cRow, 2.0),
						    new VzCircle(best_det.scale/2.0,
								 new VzLines.Style(Color.BLACK, 2))));
			    if(best_det.img != null){
				vb_train.addBack(new VisChain(LinAlg.translate(0, -100),
							      new VzImage(new VisTexture(best_det.img, VisTexture.NO_MIN_FILTER), VzImage.FLIP)));
			    }
			    

			    double match_dist = (Double)best_det.dist;
			    vb_train.addBack(new VisChain(new VzText(String.format("<<cyan>>Match Dist: %2.2f", match_dist))));

			}

                    }

                    for (int i = 0; i < vbs.size(); i++) {
                        vbs.get(i).swap();
                    }
		    vb_train.swap();
                }


		{
		    VisWorld.Buffer vb = vw.getBuffer("vo");
		    if(last_vo != null){
			vb.addBack(last_vo);
		    }
		    vb.swap();
                }


		{
		    VisWorld.Buffer vb = vw.getBuffer("shore");
		    if (shore != null) {
			double [] c = shore.getCentroid();
			VzCylinder cylinder =  new VzCylinder(1.0,1000,new VzMesh.Style(Color.YELLOW));
			double[] dir = shore.getDirection();
			double theta = Math.atan2(dir[1],dir[0]);
			// rotate about the y axis to lay it down along the x axis and then rotate about z
			VisChain lineChain = new VisChain(LinAlg.translate(c[0],image_height - c[1]),
							  LinAlg.rotateZ(theta), LinAlg.rotateY(Math.PI/2),cylinder);
                    
			vb.addBack(lineChain);
		    }
                    vb.swap();
                }


		{
		    VisWorld.Buffer vb = vw.getBuffer("region");
		    if(currentRegion != null){
			int sx = currentRegion.x1 - currentRegion.x0;
			int sy = currentRegion.y1 - currentRegion.y0;
			vb.addBack(new VisChain(LinAlg.translate(currentRegion.x0, currentRegion.y0),
						LinAlg.translate(sx/2, sy/2),
						new VzRectangle(sx, sy, new VzLines.Style(Color.CYAN, 2))));
		    }
		    vb.swap();
		}


		{
		    VisWorld.Buffer vb = vw.getBuffer("frame_rate");
		    if(frame_rate != -1 && image_width != -1){
			vb.addBack(new VisChain(LinAlg.translate(image_width - 125, 0, 0),
						new VzText(String.format("<<cyan>>Frame Rate: %2.2f", frame_rate))));
		    }
		    vb.swap();
		}


		{
		    VisWorld.Buffer vb = vw.getBuffer("stop_watch");
		    if(last_sw != null){
			VzText text = new VzText(String.format("<<cyan>>%s", last_sw.prettyPrint()));
			vb.addBack(new VisChain(LinAlg.translate(-270, 0),
						text));						
		    }
		    vb.swap();
		    
		}

		if(first_draw)
		    continue;
	    }
	}
    }

    class MouseEventHandler extends VisEventAdapter {
	boolean isSelectMode = false;

	public boolean mousePressed(VisCanvas vc, VisLayer vl, VisCanvas.RenderInfo rinfo, GRay3D ray, MouseEvent e){

            int mods = e.getModifiersEx();
            boolean ctrl = (mods & MouseEvent.CTRL_DOWN_MASK) > 0;
            if (!ctrl){
		isSelectMode = false;
		return false;
	    }


	    double xy[] = ray.intersectPlaneXY(0);
	    int x = (int)Math.round(xy[0]);
	    int y = (int)Math.round(xy[1]);
	    synchronized(lock){
		currentRegion = new Region();
		currentRegion.x0 = x;
		currentRegion.y0 = y;
		currentRegion.x1 = x;
		currentRegion.y1 = y;
	    }

	    isSelectMode = true;
	    return true;
	}

	public boolean mouseDragged(VisCanvas vc, VisLayer vl, VisCanvas.RenderInfo rinfo, GRay3D ray, MouseEvent e){
	    double xy[] = ray.intersectPlaneXY(0);
	    int x = (int)Math.round(xy[0]);
	    int y = (int)Math.round(xy[1]);

            int mods = e.getModifiersEx();
            boolean ctrl = (mods & MouseEvent.CTRL_DOWN_MASK) > 0;
            if (!ctrl || !isSelectMode) {
		return false;
	    }

	    synchronized(lock){
		if(currentRegion != null){
		    currentRegion.x1 = x;
		    currentRegion.y1 = y;
		}
	    }

	    isSelectMode = true;
	    return true;
	}

	public boolean mouseMoved(VisCanvas vc, VisLayer vl, VisCanvas.RenderInfo rinfo, GRay3D ray, MouseEvent e){

	    double xy[] = ray.intersectPlaneXY(0);
	    int x = (int)Math.round(xy[0]);
	    int y = (int)Math.round(xy[1]);

	    isSelectMode = false;
	    last_x = x;
	    last_y = y;
	    return true;
	}

	public boolean mouseReleased(VisCanvas vc, VisLayer vl, VisCanvas.RenderInfo rinfo, GRay3D ray, MouseEvent e){

	    int mods = e.getModifiersEx();
            boolean ctrl = (mods & MouseEvent.CTRL_DOWN_MASK) > 0;
            if (!ctrl || !isSelectMode) {
		isSelectMode = false;
		return false;
	    }

	    double xy[] = ray.intersectPlaneXY(0);
	    int x = (int)Math.round(xy[0]);
	    int y = (int)Math.round(xy[1]);
	    synchronized(lock){
		if(currentRegion != null){
		    currentRegion.x1 = x;
		    currentRegion.y1 = y;
		}
	    }

	    isSelectMode = false;
	    return true;
	}

    }
}
