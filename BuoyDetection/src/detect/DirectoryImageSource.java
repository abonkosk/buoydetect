package detect;
import java.awt.image.*;
import java.io.*;
import april.util.*;
import javax.imageio.*;

public class DirectoryImageSource implements ImageSource, ParameterListener {
    ParameterGUI pg;
    String directory;
    File[] imageFiles = null;
    int currentFile;
    
    // frame rate tracking
    long last_time = -1;
    double last_rate = -1;

    boolean init = false;
    Object lock = new Object();

    public DirectoryImageSource(String directory){
	this.directory = directory;

	imageFiles = new File(directory).listFiles();
	if(imageFiles == null || imageFiles.length == 0){
	    System.err.println("Error: A directory name must be provided!");
	    init = false;
	}
	currentFile = 0;

	// setup parameter gui
	pg = new ParameterGUI();
	pg.addButtons("prev_image", "Previous Image",
		      "next_image", "Next Image");	
	pg.addIntSlider("image_slider", "Image", 0, imageFiles.length-1, currentFile);
	pg.addListener(this);

	init = true;
    }

    public boolean start(){
	return init;
    }

    public BufferedImage getImage(){
	synchronized(lock){
	    BufferedImage img = null;
	    try {
		img = ImageIO.read(imageFiles[currentFile]);
	    } catch(IOException ex){}

	    if(img == null)
		return null;

	    if(last_time != -1){
		last_rate = (double)1000000000/(double)(System.nanoTime() - last_time);
	    }
	    last_time = System.nanoTime();
	    
	    return img;    
	}
    }

    public double getFrameRate(){
	return last_rate;
    }

    public ParameterGUI getParameterGUI(){
	return pg;
    }

    public void parameterChanged(ParameterGUI pg, String name){
	synchronized(lock){
	    if(name.equals("prev_image")){
		currentFile--;
		if(currentFile < 0)
		    currentFile = imageFiles.length - 1;
	    }
	    else if(name.equals("next_image")){
		currentFile++;
		currentFile %= imageFiles.length;
	    }
	    else if(name.equals("image_slider"))	    
		currentFile = pg.gi("image_slider");	    
	}
    }
}
