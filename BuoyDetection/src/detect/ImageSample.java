package detect;

import java.util.*;
import java.awt.image.*;

public class ImageSample {
    public double downsample; // rate at which which we have downsampled from the main image reflects scale
    public double[][] data; // double values
    
    public ImageSample(double[][] data, double downsample) {
        this.downsample = downsample;
        this.data = data;
    }
    
    public static ImageSample CreateBasedOn(ImageSample sample){
        ImageSample ret = new ImageSample(new double[sample.data.length][sample.data[0].length], sample.downsample);
	return ret;
    }
    
    public ImageSample CreateBassedOn() {
        return new ImageSample(new double[this.data.length][this.data[0].length], this.downsample);
    }

    // where x goes along the width of the image and y goes along the height
    public double[][] copyWindow(int xstart, int ystart, int xend, int yend){
        // should check bounds but 
        
	double[][] ret = new double[yend - ystart][xend - xstart];
	for(int y = ystart; y < yend; ++y){
	    for(int x = xstart; x < xend; ++x){
		ret[y-ystart][x-xstart] = data[y][x];
	    }
	}

	return ret;
    }
    
    public void printDetails() {
        System.out.println("Level: width = " + data[0].length + " height = " + data.length + " downsample rate = " + downsample);
    }
}
