/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package detect;

/**
 *
 * @author jrpeterson
 */
public class Constants {
    // constants conerning training data and labels
    public static final int UNKNOWN = -1;
    public static final int RED = 0;
    public static final int GREEN = 1;
    public static final int YELLOW = 2;
    public static final int WATER = 3;
    public static final int numRED = 66;
    public static final int numGREEN = 56;
    public static final int numYELLOW = 35;
    public static final int numWATER = 74;
    public static final int numLABELS = 4;
    public static final int[] numExamples = new int[] {numRED, numGREEN, numYELLOW, numWATER};
    public static final int[] LABELS = new int[] {RED, GREEN, YELLOW, WATER};
    public static final String[] fileLABELS = new String[] {"red", "green","yellow","water"};
    
    // Constants for multiresolution image pyramid construction
    public static final double[] IMGP_SCALERATIO = new double[] {1.5, 2.0/1.5, 2.5/2.0, 3.0/2.5, 4.0/3.0, 1.5, 1.5, 1.5};
    public static final int IMGP_NUMLEVELS = 9;
    
    // Constants for filter modes
    public static final int MODENEGATIVE = 0; // do we want positive or negative numbers
    public static final int MODEPOSITIVE = 1;
    public static final int MODEBOTH = 2;
    public static final int MAGMODEHIGH = 1; // do we want large magnitude values or small
    public static final int MAGMODELOW = 0; 
    
    // Constants for multi-scale nonmaximum/nonminimum suppression
    public static final double MAXRESPONSETHREOLD_LofG = 4;
    public static final double MINRESPONSETHRESHOLD_LofG = -4;
    public static final double MULTISCALERADIUS = 20;
    
    // Constants for LofG filter
    public static final double LOFGSIGMA = 2.0;
    
    // Constants for Magnitude of Gradient template filter
    public static final int GRADCANONICALWIDTH = 15;
    public static final int GRADCANONICALHEIGHT = 15;
    public static final boolean MAGOFGRADTEMPLATEMODE = true;
    public static final double MINRESPONSETHRESHOLD_MAGOFGRAD = Double.MAX_VALUE;
    
    // Constants for EigenBuoy detector
    public static final int EIGENCANONICALWIDTH = 15; // dimensions to convert images to
    public static final int EIGENCANONICALHEIGHT = 15;
    public static final int EIGENCOMPONENTS = 20;  // how many eigenbuoys are we going to keep around?
    public static final double RESPONSETHRESHOLD_EIGENFILTER = 10;
    
    // Camera constants currently unknown expected in pixels per meter
    public static double FOCALLENGTHROW0 = 555.9765594;   // y
    public static double FOCALLENGTHCOL0 = 554.5847439;   // x
    public static double OPTICALCENTERROW0 = 253.2243849; // y
    public static double OPTICALCENTERCOL0 = 381.0534094; // x
    public static double CAMERAHEIGHT = 0.5842; // 23 inches above the water
    public static double BUOYHEIGHT = 0.1015; // buoy centroid estimated to be half diameter above the water
    public static double BUOYDIAMETER = 0.203; // buoys are the A0 spec https://www.polyformus.com/c-8-a-series.aspx
    public static double SCALETOLERANCE = 0.5; // accept variation +/- this percent 
    public static double PITCHCORRECTION = -0.06;  // camera is not level with the water this was hand adjusted
    public static double ROLLCORRECTION = 0.03;
    // ignoring some of the other effects for now
    
    // Constants for RANSAC for shore detection
    public static int RANSACITTERATIONS = 100;
    public static double RANSACPTHRESHOLD = 10; // magic threshold for inlier vs not inlier Perpendicular Distance
    //public static double RANSACCTHRESHOLD = 35; // threshold for distance from point to centroid, should be obtained
    public static int RANSACMINLINESIZE = 3; // minimum points we will allow before we stop trying to make new lines
    
    // Constants for RGBGradientDescent
    public static int GRADDESCENTMAXITERATIONS = 30;
    public static double GRADDESCENTERRORTHRESHOLD = 0.0001;
    
    // Constants for Histogram based clasifier
    public static int NUMBUCKETS = 10; //8; //7; //6; //5;
}
