/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package detect;

import Utility.ImageProc;
import Utility.KdNode;
import Utility.KdTree;
import Utility.Matrix;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import april.util.ParameterGUI;

/**
 *
 * @author jrpeterson
 */
public class RGBNearestNeighbor implements Classifier {
    // canonical width and height that images will be scaled to before extracting color data
    // not particularly important
    ArrayList<KdNode> nodes;
    KdTree tree;
    
    double[] waterRGB; // lets try to filter out water!
    
    public RGBNearestNeighbor() {
        nodes = new ArrayList<KdNode>();
        waterRGB = new double[3];
        System.out.println("Begining RGBClassifier training");
        String dir = "../buoys/trainingdata/";
        // for each category red, green, blue, water
        for (int c = 0; c < Constants.numLABELS; c++) {
            // read in each training example
            System.out.println("Training " + Constants.fileLABELS[c]);
            for (int i = 1; i <= Constants.numExamples[c]; i++) {
                String fileName = dir + Constants.fileLABELS[c] + Integer.toString(i) + ".png";
                // acquire and set image
                BufferedImage img = null;
                try {
                    img = ImageIO.read(new File(fileName));
                } catch (IOException ex) {
                    System.err.println("Error: Could not read a BufferedImage from file '" + fileName + "'");
                    System.exit(-1);
                }
                
                // get the average red green and blue across the example
                double[] rgb = ImageProc.extractRGB(img);
                
                // normalize by brightness
                //LinAlg.normalizeEquals(rgb);
                
                if (c == Constants.WATER) {
                    waterRGB = Matrix.add(waterRGB, rgb);
                }
                //System.out.println(Constants.fileLABELS[c] + " ");
                //Matrix.print(rgb);
                // create a KDNode for it
                KdNode kdnode = new KdNode(rgb,Constants.LABELS[c]);
		kdnode.setNodeData("dist", kdnode.distance(rgb));
		kdnode.setNodeData("image", img);
                nodes.add(kdnode);
            }
        }
        tree = new KdTree(nodes);
        System.out.println("Training Complete: created " + nodes.size() + " nodes");
        waterRGB = Matrix.times(1.0/Constants.numWATER, waterRGB);
    }
    
    @Override
    public int classify(BufferedImage img, Detection d) {
        double[] rgb = ImageProc.extractRGB(img,d.cRow,d.cCol,d.scale);
        //LinAlg.normalizeEquals(rgb); // scale brightness
        
	KdNode node = tree.Nearest(rgb);
        d.type = node.getData();
	d.img = (BufferedImage)node.getNodeData("image");
	d.dist = node.distance(rgb);
        //System.out.println("classifing row = " + d.cRow + " col = " + d.cCol + " scale = " + d.scale + " as " + Constants.fileLABELS[d.type]);
        return d.type;
    }
    
    // note that the returned rgb has been normalized
    public double[] getNearest(BufferedImage img, Detection d) {
        double[] rgb = ImageProc.extractRGB(img,d.cRow,d.cCol,d.scale);
        
        return tree.Nearest(rgb).getCoord();
    }


    @Override
    public ParameterGUI getParameterGUI() {
      return null;
    }
    
    // where scale is how many pixels square should it average over
    // where row and column are the center to be averaged around
    
    
    private static double[] extractRGB(int[][][] rgbimg) {
        double[] rgb = new double[3];
        int total = rgbimg[0].length * rgbimg[0][0].length;
        
        for (int c = 0; c < rgb.length; c++) {
            for (int i = 0; i < rgbimg[c].length; i++) {
                for (int j = 0; j < rgbimg[c][0].length; j++) {
                    rgb[c] += rgbimg[c][i][j];
                }
            }
            rgb[c] /= total;
        }
        return rgb;
    }
    
    
    // converts color image to 3 2D arrays
    private static int[][][] convertImageRGB(BufferedImage img) {
        int[][] red = new int[img.getHeight()][img.getWidth()];
        int[][] green = new int[img.getHeight()][img.getWidth()];
        int[][] blue = new int[img.getHeight()][img.getWidth()];
        
        if (img.getType() == BufferedImage.TYPE_3BYTE_BGR) { // type 5
            byte data[] = ((DataBufferByte) (img.getRaster().getDataBuffer())).getData();
            int k;
            byte r, g, b;
            for (int i = 0; i < img.getHeight(); i++) {
                for (int j = 0; j < img.getWidth(); j++) {
                    k = 3 * (i * img.getWidth() + j);
                    b = data[k];
                    g = data[k + 1];
                    r = data[k + 2];
                    // inline for more speed!!
                    red[i][j] = ((int) r & 0xff);
                    green[i][j] = ((int) g & 0xff);
                    blue[i][j] = ((int) b & 0xff);
                }
            }
        } else if (img.getType() == BufferedImage.TYPE_INT_RGB) { // type 1
            int data[] = ((DataBufferInt) (img.getRaster().getDataBuffer())).getData();

            int k;
            int r, g, b, rgb;
            for (int i = 0; i < img.getHeight(); i++) {
                for (int j = 0; j < img.getWidth(); j++) {
                    rgb = data[i*img.getWidth() + j];
                    red[i][j] = (rgb & 0xff0000) >> 16;
                    green[i][j] = (rgb & 0xff00) >> 8;
                    blue[i][j] = (rgb & 0xff);
                }
            }
        } else {
            System.out.println("Unsupported image format! = " + img.getType());
            assert false;
            return null;
        }
        
        return new int[][][] {red,green,blue};
    }
}
