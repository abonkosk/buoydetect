/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package detect;

import Utility.KdNode;
import Utility.KdTree;
import Utility.Matrix;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import april.util.ParameterGUI;

/**
 *
 * @author jrpeterson
 */
public class HSVNearestNeighbor implements Classifier {
    // canonical width and height that images will be scaled to before extracting color data
    // not particularly important
    ArrayList<KdNode> nodes;
    KdTree tree;

    public HSVNearestNeighbor()  {
        nodes = new ArrayList<KdNode>();
        System.out.println("Begining RGBClassifier training");
        String dir = "../buoys/trainingdata/";
        // for each category red, green, blue, water
        for (int c = 0; c < Constants.numLABELS; c++) {
            // read in each training example
            System.out.println("Training " + Constants.fileLABELS[c]);
            for (int i = 1; i <= Constants.numExamples[c]; i++) {
                String fileName = dir + Constants.fileLABELS[c] + Integer.toString(i) + ".png";
                // acquire and set image
                BufferedImage img = null;
                try {
                    img = ImageIO.read(new File(fileName));
                } catch (IOException ex) {
                    System.err.println("Error: Could not read a BufferedImage from file '" + fileName + "'");
                    System.exit(-1);
                }

                // get the average red green and blue across the example
                double[] hsv = extractHSV(img);
                //System.out.println(Constants.fileLABELS[c] + " ");
                //Matrix.print(hsv);
                // create a KDNode for it
                // throw out value?
                KdNode kdnode = new KdNode(new double[] {hsv[0], hsv[1]},Constants.LABELS[c]);
                nodes.add(kdnode);
            }
        }
        tree = new KdTree(nodes);
        System.out.println("Training Complete: created " + nodes.size() + " nodes");
    }

    @Override
    public int classify(BufferedImage img, Detection d) {
        d.type = classify(img,d.cRow,d.cCol,d.scale);
        //System.out.println("classifing row = " + d.cRow + " col = " + d.cCol + " scale = " + d.scale + " as " + Constants.fileLABELS[d.type]);
        return d.type;
    }

    // where scale is how many pixels square should it average over
    // where row and column are the center to be averaged around
    public int classify(BufferedImage img, int row, int column, double scale) {
        double red = 0;
        double green = 0;
        double blue = 0;
        int total = 0; // total pixels averaged over
        int s2 = (int) Math.round(scale/2.0);

        int istart,iend,jstart,jend;
        if (row < s2) {
            istart = 0;
        } else {
            istart = row - s2;
        }

        if (column < s2) {
            jstart = 0;
        } else {
            jstart = column - s2;
        }

        if ((row+s2) >= img.getHeight()) {
            iend = img.getHeight();
        } else {
            iend = row+s2;
        }

        if ((column+s2) >= img.getWidth()) {
            jend = img.getWidth();
        } else {
            jend = column+s2;
        }

        if (img.getType() == BufferedImage.TYPE_3BYTE_BGR) { // type 5
            byte data[] = ((DataBufferByte) (img.getRaster().getDataBuffer())).getData();


            int k;
            byte r, g, b;
            for (int i = istart; i < iend; i++) {
                for (int j = jstart; j < jend; j++) {
                    k = 3 * (i * img.getWidth() + j);
                    b = data[k];
                    g = data[k + 1];
                    r = data[k + 2];
                    // inline for more speed!!
                    red += (double) ((int) r & 0xff);
                    green += (double) ((int) g & 0xff);
                    blue += (double) ((int) b & 0xff);
                    total++;
                }
            }
        } else if (img.getType() == BufferedImage.TYPE_4BYTE_ABGR) { // type 6
            byte data[] = ((DataBufferByte) (img.getRaster().getDataBuffer())).getData();

            int k;
            byte r, g, b;
            for (int i = 0; i < img.getHeight(); i++) {
                for (int j = 0; j < img.getWidth(); j++) {
                    k = 4 * (i * img.getWidth() + j);
                    b = data[k + 1];
                    g = data[k + 2];
                    r = data[k + 3];
                    // inline for more speed!!
                    red += (double) ((int) r & 0xff);
                    green += (double) ((int) g & 0xff);
                    blue += (double) ((int) b & 0xff);
                    total++;
                }
            }
        } else if (img.getType() == BufferedImage.TYPE_INT_RGB) { // type 1
            int data[] = ((DataBufferInt) (img.getRaster().getDataBuffer())).getData();

            int k;
            int r, g, b, rgb;
            for (int i = istart; i < iend; i++) {
                for (int j = jstart; j < jend; j++) {
                    rgb = data[i*img.getWidth() + j];
                    r = (rgb & 0xff0000) >> 16;
                    g = (rgb & 0xff00) >> 8;
                    b = (rgb & 0xff);
                    // inline for more speed!!
                    red += (double) r;
                    green += (double) g;
                    blue += (double) b;
                    total++;
                }
            }
        } else {
            System.out.println("Unsupported image format! = " + img.getType());
            assert false;
            return 0;
        }
        //double totalvalue = red + green + blue; // lets try to normalize by ratio of color
        double[] rgb = new double[] {red/total, green/total, blue/total};
        double[] hsv = RGBtoHSV(rgb);
        //System.out.print("attempting to classify ");
        //Matrix.print(hsv);
        // TODO add range check and return unknown for large distances
        return (tree.Nearest(new double[] {hsv[0],hsv[1]})).getData();
    }

    @Override
    public ParameterGUI getParameterGUI(){
        return null;
    }

    private static double[] extractHSV(int[][][] rgbimg) {
        double[] rgb = new double[3];
        int total = rgbimg[0].length * rgbimg[0][0].length;

        for (int c = 0; c < rgb.length; c++) {
            for (int i = 0; i < rgbimg[c].length; i++) {
                for (int j = 0; j < rgbimg[c][0].length; j++) {
                    rgb[c] += rgbimg[c][i][j];
                }
            }
            rgb[c] /= total;
        }
        return RGBtoHSV(rgb);
    }

    private static double[] extractHSV(BufferedImage img) {
        double red = 0;
        double green = 0;
        double blue = 0;
        int total = 0;
        //System.out.println("image types " + BufferedImage.TYPE_4BYTE_ABGR + ", " + BufferedImage.TYPE_INT_ARGB);
        if (img.getType() == BufferedImage.TYPE_3BYTE_BGR) { // type 5
            byte data[] = ((DataBufferByte) (img.getRaster().getDataBuffer())).getData();

            int k;
            byte r, g, b;
            for (int i = 0; i < img.getHeight(); i++) {
                for (int j = 0; j < img.getWidth(); j++) {
                    k = 3 * (i * img.getWidth() + j);
                    b = data[k];
                    g = data[k + 1];
                    r = data[k + 2];
                    // inline for more speed!!
                    red += (double) ((int) r & 0xff);
                    green += (double) ((int) g & 0xff);
                    blue += (double) ((int) b & 0xff);
                    total++;
                }
            }
        } else if (img.getType() == BufferedImage.TYPE_4BYTE_ABGR) { // type 6
            byte data[] = ((DataBufferByte) (img.getRaster().getDataBuffer())).getData();

            int k;
            byte r, g, b;
            for (int i = 0; i < img.getHeight(); i++) {
                for (int j = 0; j < img.getWidth(); j++) {
                    k = 4 * (i * img.getWidth() + j);
                    b = data[k + 1];
                    g = data[k + 2];
                    r = data[k + 3];
                    // inline for more speed!!
                    red += (double) ((int) r & 0xff);
                    green += (double) ((int) g & 0xff);
                    blue += (double) ((int) b & 0xff);
                    total++;
                }
            }
        } else if (img.getType() == BufferedImage.TYPE_INT_RGB) { // type 1
            int data[] = ((DataBufferInt) (img.getRaster().getDataBuffer())).getData();

            int k;
            int r, g, b, rgb;
            for (int i = 0; i < img.getHeight(); i++) {
                for (int j = 0; j < img.getWidth(); j++) {
                    rgb = data[i*img.getWidth() + j];
                    r = (rgb & 0xff0000) >> 16;
                    g = (rgb & 0xff00) >> 8;
                    b = (rgb & 0xff);
                    // inline for more speed!!
                    red += (double) r;
                    green += (double) g;
                    blue += (double) b;
                    total++;
                }
            }
        } else {
            System.out.println("Unsupported image format! = " + img.getType());
            assert false;
            return null;
        }
        //double totalvalue = red + green + blue; // lets try to normalize by ratio of color
        double[] rgb = new double[] {red/total, green/total, blue/total};
        //return new double[] {red/total, green/total, blue/total};
        return RGBtoHSV(rgb);
    }

    // converts color image to 3 2D arrays
    private static int[][][] convertImageRGB(BufferedImage img) {
        int[][] red = new int[img.getHeight()][img.getWidth()];
        int[][] green = new int[img.getHeight()][img.getWidth()];
        int[][] blue = new int[img.getHeight()][img.getWidth()];

        if (img.getType() == BufferedImage.TYPE_3BYTE_BGR) { // type 5
            byte data[] = ((DataBufferByte) (img.getRaster().getDataBuffer())).getData();
            int k;
            byte r, g, b;
            for (int i = 0; i < img.getHeight(); i++) {
                for (int j = 0; j < img.getWidth(); j++) {
                    k = 3 * (i * img.getWidth() + j);
                    b = data[k];
                    g = data[k + 1];
                    r = data[k + 2];
                    // inline for more speed!!
                    red[i][j] = ((int) r & 0xff);
                    green[i][j] = ((int) g & 0xff);
                    blue[i][j] = ((int) b & 0xff);
                }
            }
        } else if (img.getType() == BufferedImage.TYPE_INT_RGB) { // type 1
            int data[] = ((DataBufferInt) (img.getRaster().getDataBuffer())).getData();

            int k;
            int r, g, b, rgb;
            for (int i = 0; i < img.getHeight(); i++) {
                for (int j = 0; j < img.getWidth(); j++) {
                    rgb = data[i*img.getWidth() + j];
                    red[i][j] = (rgb & 0xff0000) >> 16;
                    green[i][j] = (rgb & 0xff00) >> 8;
                    blue[i][j] = (rgb & 0xff);
                }
            }
        } else {
            System.out.println("Unsupported image format! = " + img.getType());
            assert false;
            return null;
        }

        return new int[][][] {red,green,blue};
    }

    // convert RGB to HSV
    private static double[] RGBtoHSV(double[] rgb) {
        double[] hsv = new double[3];
        //Get individual values for convenience
        double red = rgb[0];
        double green = rgb[1];
        double blue = rgb[2];

        //Compute the max, min and delta values in RGB space
        double min = red < green ? (red < blue ? red : blue) : (green < blue ? green : blue);
        double max = red > green ? (red > blue ? red : blue) : (green > blue ? green : blue);
        double delta = max - min;

        //The "Value" of HSV is simply the max RGB value
        hsv[2] = max;

        //Compute the saturation
        if (max != 0.0) {
            hsv[1] = delta / max;
        } else {
            //This only happens when the RGB values are {0, 0, 0}
            //In this case, Saturation is 0 and the Hue is undefined
            hsv[1] = 0;
            hsv[0] = -1;
        }

        //Compute the hue
        if (red == max) {
            //When r is the max, the hue is somewhere between yellow and magenta
            hsv[0] = (green - blue) / delta;
        } else if (green == max) {
            //When g is the max, the hue is somewhere between cyan and yellow
            hsv[0] = 2 + (blue - red) / delta;
        } else {
            //When b is the max, the hue is somewhere between magenta and cyan
            hsv[0] = 4 + (red - green) / delta;
        }
        hsv[0] *= 60;

        //Make sure hue is between 0 and 360 degrees
        if (hsv[0] < 0) {
            hsv[0] += 360;
        }
        
        return hsv;
    }

    @Override
    public double[] getNearest(BufferedImage img, Detection d) {
        throw new UnsupportedOperationException("Not supported yet.");
    }


}

