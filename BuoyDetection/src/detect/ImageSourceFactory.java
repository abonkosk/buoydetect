package detect;

public class ImageSourceFactory {

    public static ImageSource getSourceFromArgs(String[] args){
	if(args.length < 1)	    
	    return null;

	// directory images
	if(args[0].equals("-f")){
	    if(args.length < 2){
		System.err.println("Error: expected a directory name after '-f'");
		return null;
	    }

	    return new DirectoryImageSource(args[1]);
	}

	// lcm images
	else if(args[0].equals("-lcm")){
	    return new LCMImageSource("RAW_CAMERA1");
	}

	else {
	    System.err.println("Error: could not create ImageSource from commandline args...");
	    return null;
	}

    }

}
