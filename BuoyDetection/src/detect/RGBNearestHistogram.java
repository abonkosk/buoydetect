/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package detect;

import Utility.ImageProc;
import Utility.KdNode;
import Utility.KdTree;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import april.util.ParameterGUI;

/**
 *
 * @author jrpeterson
 * 
 * computes nearest neighbor in a normalized histogram space, histograms are not lighting normalized
 */
public class RGBNearestHistogram implements Classifier{
    
    ArrayList<KdNode> nodes;
    KdTree tree;
    
    public RGBNearestHistogram() {
        nodes = new ArrayList<KdNode>();
        System.out.println("Begining RGBHistClassifier training");
        String dir = "../buoys/trainingdata/";
        // for each category red, green, blue, water
        for (int c = 0; c < Constants.numLABELS; c++) {
            // read in each training example
            System.out.println("Training " + Constants.fileLABELS[c]);
            for (int i = 1; i <= Constants.numExamples[c]; i++) {
                String fileName = dir + Constants.fileLABELS[c] + Integer.toString(i) + ".png";
                // acquire and set image
                BufferedImage img = null;
                try {
                    img = ImageIO.read(new File(fileName));
                } catch (IOException ex) {
                    System.err.println("Error: Could not read a BufferedImage from file '" + fileName + "'");
                    System.exit(-1);
                }
                
                // get the average red green and blue across the example
                double[] rgbhist = ImageProc.extractRGBHistogram(img);
                
                // create a KDNode for it
                KdNode kdnode = new KdNode(rgbhist,Constants.LABELS[c]);
                nodes.add(kdnode);
            }
        }
        tree = new KdTree(nodes);
        System.out.println("Training Complete: created " + nodes.size() + " nodes");
    }

    @Override
    public int classify(BufferedImage img, Detection d) {
        double[] rgbhist = ImageProc.extractRGBHistogram(img,d.cRow,d.cCol,d.scale);
        //LinAlg.normalizeEquals(rgb); // scale brightness
        
        d.type = tree.Nearest(rgbhist).getData();
        //System.out.println("classifing row = " + d.cRow + " col = " + d.cCol + " scale = " + d.scale + " as " + Constants.fileLABELS[d.type]);
        return d.type;
    }

    @Override
    // returns the histogram, not particularly useful
    public double[] getNearest(BufferedImage img, Detection d) {
        double[] rgbhist = ImageProc.extractRGBHistogram(img,d.cRow,d.cCol,d.scale);
        
        return tree.Nearest(rgbhist).getCoord();
    }

    @Override
    public ParameterGUI getParameterGUI(){
        return null;
    }
}
