package Utility;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author jrpeterson
 *
 * For the construction and use of the k-d tree, a useful space partitioning data structure
 * in k dimensional space
 * 
 * TODO: modify to take in a random seed so that can use common random numbers across trials
 */
public class KdTree {

    private int dimension; // the dimension that the points in the KdNodes live in this is not 0 indexed
    private int maxDepth; // max depth that the tree extends to
    // TODO how to keep track of max depth when removing nodes?
    private int size; // the number total number of nodes contained within the tree
    private KdNode root;

    // constructs KdTree
    public KdTree(ArrayList<KdNode> nodes) {
        if (!nodes.isEmpty()) {

            this.size = nodes.size();
            dimension = nodes.get(0).getDimension();

            root = construct(nodes, 0);
        } else {
            root = null;
            dimension = -1;
            maxDepth = -1;
            size = 0;
        }
    }

    //consistency check that nodes are being placed correctly
    public boolean check() {
        return check(0, root);
    }

    /* checks the consistency of the Kd tree */
    private boolean check(int depth, KdNode current) {
        int splitAxis = depth % dimension;
        //System.out.println("checking at depth " + depth);

        if (current != null) {
            KdNode leftchild, rightchild;

            // left child
            leftchild = current.getLeftChild();
            if (leftchild != null) {
                // check that less along the split axis
                if (!(leftchild.getCoord(splitAxis) < current.getCoord(splitAxis))) {
                    System.out.println("Incorrect Split");
                    return false;
                }
                if (leftchild.getDepth() != depth + 1) {
                    System.out.println("Incorrect Depth");
                    return false;
                }
            }

            // right child
            rightchild = current.getRightChild();
            if (rightchild != null) {
                // check that right child is greater or equall along split axis
                if (!(rightchild.getCoord(splitAxis) >= current.getCoord(splitAxis))) {
                    System.out.println("Incorrect Split");
                    return false;
                }
                if (rightchild.getDepth() != depth + 1) {
                    System.out.println("Incorrect Depth");
                    return false;
                }
            }

            // recurse and check both left and right children
            return check(depth + 1, leftchild) && check(depth + 1, rightchild);

        } else {
            return true;
        }
    }

    // adds an elemnt to the tree and returns the depth at which it was added
    public int add(KdNode node) {
        if (!isEmpty()) {
            assert node.getDimension() == dimension : "Warning incompatible dimensions";

            KdNode parent = dive(root, node);

            // have to repeat calculation to figure out which side it belongs on
            int splitAxis = parent.getDepth() % dimension;
            if (node.getCoord(splitAxis) < parent.getCoord(splitAxis)) {
                assert parent.getLeftChild() == null : "Warning dive has failed";
                parent.setLeftChild(node);
            } else {
                assert parent.getRightChild() == null : "Warning dive has failed";
                parent.setRightChild(node);
            }

            // clean up a few last things
            node.setDepth(parent.getDepth() + 1);
            if (node.getDepth() > maxDepth) {
                maxDepth = node.getDepth();
            }
            this.size++;

            return node.getDepth();
            // deal with empty case
        } else {
            root = node;
            dimension = node.getDimension();
            size = 1;
            maxDepth = 0;
            node.setDepth(0);
            return node.getDepth();
        }
    }

    /*
     * recursively descends the tree and passes up the appropriate parrent node for the payload
     */
    private KdNode dive(KdNode current, KdNode payload) {
        int splitAxis = current.getDepth() % dimension;
        if (payload.getCoord(splitAxis) < current.getCoord(splitAxis)) {
            if (current.getLeftChild() != null) {
                return dive(current.getLeftChild(), payload);
            } else {
                return current;
            }
        } else {
            if (current.getRightChild() != null) {
                return dive(current.getRightChild(), payload);
            } else {
                return current;
            }
        }
    }

    /* removes all instances of the target from the tree and reconstructs the tree
     * returns success if it found an instance
     * maybe slow since has reconstruct tree the number of instances it found
     */
    public boolean remove(KdNode target) {

        boolean Found = false;

        // check special case where the root node is the target
        if (root == target) {
            ArrayList<KdNode> orphans = new ArrayList<KdNode>();
            collectChildren(root, orphans);
            root = construct(orphans, 0);
            size--;
            Found = true;
        }

        // get all the parents of the target
        ArrayList<KdNode> targetParents = new ArrayList<KdNode>();
        find(root, target, targetParents);

        if (!targetParents.isEmpty()) {
            // for each parent
            for (KdNode parent : targetParents) {
                // which one do we want to get rid of
                if (target == parent.getLeftChild()) {
                    // left side
                    // collect all the nodes that would be orphaned by the removal
                    ArrayList<KdNode> orphans = new ArrayList<KdNode>();
                    collectChildren(parent.getLeftChild(), orphans);
                    parent.setLeftChild(construct(orphans, parent.getDepth() + 1));
                    size--;
                } else {
                    // right side
                    // collect all the nodes that would be orphaned by the removal
                    ArrayList<KdNode> orphans = new ArrayList<KdNode>();
                    collectChildren(parent.getRightChild(), orphans);
                    parent.setRightChild(construct(orphans, parent.getDepth() + 1));
                    size--;
                }
            }
            Found = true;
        }
        return Found;
    }

    /*
     * recursively descend the tree and find all instances of parents of the target node
     */
    private void find(KdNode current, KdNode target, ArrayList<KdNode> parents) {
        if ((parents != null) && (current != null)) {
            if ((current.getLeftChild() == target) || (current.getRightChild() == target)) {
                parents.add(current);
            }
            // recurse for left and right children
            find(current.getLeftChild(), target, parents);
            find(current.getRightChild(), target, parents);
        }
    }

    // recursively collects all of the nodes below the current node
    // need to give this function an arrayList to put all the children in
    private void collectChildren(KdNode current, ArrayList<KdNode> children) {
        if (children != null) {
            KdNode leftchild, rightchild;
            // left children
            leftchild = current.getLeftChild();
            if (leftchild != null) {
                children.add(leftchild);
                collectChildren(leftchild, children);
            }
            // right children
            rightchild = current.getRightChild();
            if (rightchild != null) {
                children.add(rightchild);
                collectChildren(rightchild, children);
            }
        }
    }

    /*
     * nearest neighbor search, returns the node closest to the given coordinates
     */
    public KdNode Nearest(double[] coordinates) {
        assert coordinates.length == dimension : "Warning incompatible dimensions";
        ArrayList<KdNode> nearest = kNearest(1, coordinates);
        if (nearest.isEmpty()) {
            return null;
        } else {
            return nearest.get(0);
        }
    }
    /*
     * k nearest neighbor returns the k closest nodes to the given coordinates
     */

    public ArrayList<KdNode> kNearest(int k, double[] coordinates) {
        assert coordinates.length == dimension : "Warning incompatible dimensions";
        ArrayList<KdNode> knearest = new ArrayList<KdNode>();
        ArrayList<Double> distance2 = new ArrayList<Double>();

        kNearestR(root, k, coordinates, knearest, distance2);

        return knearest;
    }

    /*
     * recursive formulation of kNearest
     * where current is the current position in the tree
     * k is the number of neighbors to keep
     * knearest stores the neighbors
     * distance2 stores their distances
     * 
     * TODO revise the for loop checking the whether the hyperspehere extends across,
     * its sorted so only should need to check the last element
     */
    private void kNearestR(KdNode current, int k, double[] coordinates, ArrayList<KdNode> knearest, ArrayList<Double> distance2) {

        assert ((knearest != null) && (distance2 != null) && (coordinates.length == dimension)) : "Warning!";

        // dive down the tree until reach leaf node, then back up
        if (current != null) {
            int splitAxis = current.getDepth() % dimension;
            // left side
            if (coordinates[splitAxis] < current.getCoord(splitAxis)) {
                kNearestR(current.getLeftChild(), k, coordinates, knearest, distance2);

                // check the distance of the current node
                double d2cur = current.distance2(coordinates);
                // check if this is closer than what we have so far
                int pos = orderedInsert(distance2, d2cur, k);
                // if was better
                if (pos < k) {
                    insertTruncate(knearest, current, pos, k);
                }

                // now check to see if the hypersphere extends across the splitting hyperplane for this level
                double d2split = (current.getCoord(splitAxis) - coordinates[splitAxis])
                        * (current.getCoord(splitAxis) - coordinates[splitAxis]);
                // compare to the worst distance we have so far
                if (distance2.get(distance2.size() - 1) >= d2split) {
                    // if the hypersphere extended across then need to do the otherside
                    kNearestR(current.getRightChild(), k, coordinates, knearest, distance2);
                }
                /*****************************************************************************/
            } else {
                kNearestR(current.getRightChild(), k, coordinates, knearest, distance2);

                // check the distance of the current node
                double d2cur = current.distance2(coordinates);
                // check if this is closer than what we have so far
                int pos = orderedInsert(distance2, d2cur, k);
                // if was better
                if (pos < k) {
                    insertTruncate(knearest, current, pos, k);
                }

                // now check to see if the hypersphere extends across the splitting hyperplane for this level
                double d2split = (current.getCoord(splitAxis) - coordinates[splitAxis])
                        * (current.getCoord(splitAxis) - coordinates[splitAxis]);
                // compare to the worst distance we have so far
                if (distance2.get(distance2.size() - 1) >= d2split) {
                    // if the hypersphere extended across then need to do the otherside
                    kNearestR(current.getLeftChild(), k, coordinates, knearest, distance2);
                }
            }
        }
    }

    /* 
     * inserts node at the specified position and truncates the length of the list to k
     */
    private void insertTruncate(ArrayList<KdNode> nodes, KdNode newNode, int pos, int k) {
        nodes.add(pos, newNode);
        int initialSize = nodes.size();
        // trim to size
        for (int i = k; i < initialSize; i++) {
            nodes.remove(k);
        }
    }
    /* inserts an element into the list to keep it ordered
     * returns where it was inserted
     * keeps the list of the specified length
     */

    private int orderedInsert(ArrayList<Double> values, double newValue, int k) {
        int pos = 0;
        // find where it belongs
        while ((pos < values.size()) && (values.get(pos) < newValue)) {
            pos++;
        }
        // handle case where beyond the current list
        if (pos >= values.size()) {
            // if we are below number we should have add it
            if (pos < k) {
                values.add(newValue);
            }
            return pos;
        } else {
            values.add(pos, newValue);
            // now remove all the elements beyond the edge of the list
            int initialSize = values.size();
            for (int i = k; i < initialSize; i++) {
                values.remove(k);
            }
            return pos;
        }
    }

    // returns all points within (inclusive) the given radius performs no sorting on the points
    public ArrayList<KdNode> Near(double[] coordinates, double radius) {
        
        assert coordinates.length == dimension : "Warning incompatible dimensions";
        
        ArrayList<KdNode> near = new ArrayList<KdNode>();
        NearR(root,coordinates,radius*radius,near);
        
        return near;
    }

    /*
     * recursive formulation of the Near problem, expects the squared distance for efficiency
     */
    private void NearR(KdNode current, double[] coordinates, double radius2, ArrayList<KdNode> near) {

        assert ((near != null) && (coordinates.length == dimension)) : "Warning!";

        // dive down the tree until reach leaf node, then back up
        if (current != null) {
            int splitAxis = current.getDepth() % dimension;
            // left side
            if (coordinates[splitAxis] < current.getCoord(splitAxis)) {
                NearR(current.getLeftChild(), coordinates, radius2, near);

                // check the distance of the current node
                double d2cur = current.distance2(coordinates);
                // if close enough add it
                if (d2cur <= radius2) {
                    near.add(current);
                }

                // now check to see if the hypersphere extends across the splitting hyperplane for this level
                double d2split = (current.getCoord(splitAxis) - coordinates[splitAxis])
                        * (current.getCoord(splitAxis) - coordinates[splitAxis]);
                // compare to our near radius
                if (radius2 >= d2split) {
                    // if the hypersphere extended across then need to do the otherside
                    NearR(current.getRightChild(), coordinates, radius2, near);
                }
                /*****************************************************************************/
            } else {
                NearR(current.getRightChild(), coordinates, radius2, near);

                // check the distance of the current node
                double d2cur = current.distance2(coordinates);
                // if close enough add it
                if (d2cur <= radius2) {
                    near.add(current);
                }

                // now check to see if the hypersphere extends across the splitting hyperplane for this level
                double d2split = (current.getCoord(splitAxis) - coordinates[splitAxis])
                        * (current.getCoord(splitAxis) - coordinates[splitAxis]);
                // compare to our near radius
                if (radius2 >= d2split) {
                    // if the hypersphere extended across then need to do the otherside
                    NearR(current.getLeftChild(), coordinates, radius2, near);
                }
            }
        }
    }

    /*
     * Redesigned recursive construction of the Kd tree 
     * where depth level of the root in each level of recursion
     * split axis is the current axis that we are splitting the group around
     */
    private KdNode construct(ArrayList<KdNode> nodes, int depth) {

        if (!nodes.isEmpty()) {

            int splitAxis = depth % dimension;

            // pick which one to branch off of
            KdNode Lroot = SelectMedian(nodes, splitAxis);
            if (!nodes.remove(Lroot)) {
                System.out.println("Warning something has gone very wrong!");
            }

            // set depth
            Lroot.setDepth(depth);
            if (depth > maxDepth) {
                maxDepth = depth;
            }

            // separate nodes into two groups
            ArrayList<KdNode> leftchildren = new ArrayList<KdNode>();
            ArrayList<KdNode> rightchildren = new ArrayList<KdNode>();
            for (KdNode child : nodes) {
                if (child.getCoord(splitAxis) < Lroot.getCoord(splitAxis)) {
                    leftchildren.add(child);
                } else {
                    rightchildren.add(child);
                }
            }

            // now recurse with the two groups
            // left group
            Lroot.setLeftChild(construct(leftchildren, depth + 1));
            Lroot.setRightChild(construct(rightchildren, depth + 1));

            return Lroot;
        } else {
            return null;
        }
    }

    public int getMaxDepth() {
        return maxDepth;
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        if (size == 0) {
            return true;
        } else {
            return false;
        }
    }

    /*
     * recursively performs median of medians to approximate the median of the given nodes in the selected axis
     * gives median accurately for number of nodes < 5
     *
     * the approximation becomes most obvious for
     */
    private KdNode SelectMedian(List<KdNode> nodes, int axis) {

        assert (axis < dimension) && (axis >= 0) : "Warning: Invalid Axis";

        // handle all of the cases
        if (nodes.isEmpty()) {
            return null;
        }
        /********************************************************************/
        if (nodes.size() == 1) {
            return nodes.get(0);        // return the element
            /****************************************************************/
        } else if (nodes.size() == 2) {
            // returns random node if only 2 available
            Random R = new Random();
            if (R.nextBoolean()) {
                return nodes.get(0);
            } else {
                return nodes.get(1);
            }
            /****************************************************************/
        } else if (nodes.size() == 3) {
            // computes the median of 3 numbers with 2 - 3 comparisons
            KdNode a, b;

            // order a < b
            if (nodes.get(0).getCoord(axis) < nodes.get(1).getCoord(axis)) {
                a = nodes.get(0);
                b = nodes.get(1);
            } else {
                b = nodes.get(0);
                a = nodes.get(1);
            }

            // comprare the 3rd to b
            if (b.getCoord(axis) < nodes.get(2).getCoord(axis)) {
                return b;
            } else if (nodes.get(2).getCoord(axis) < a.getCoord(axis)) {
                return a;
            } else {
                return nodes.get(2);
            }
            /*****************************************************************/
        } else if (nodes.size() == 4) {
            // computes the approximate median of 4 number with 4 comparisons
            KdNode a, b, c, d;
            // order a < b
            if (nodes.get(0).getCoord(axis) < nodes.get(1).getCoord(axis)) {
                a = nodes.get(0);
                b = nodes.get(1);
            } else {
                b = nodes.get(0);
                a = nodes.get(1);
            }
            // construct ordered pair cd
            if (nodes.get(2).getCoord(axis) < nodes.get(3).getCoord(axis)) {
                c = nodes.get(2);
                d = nodes.get(3);
            } else {
                d = nodes.get(2);
                c = nodes.get(3);
            }
            // compare a to c, highest could be median put candidate in a
            if (a.getCoord(axis) < c.getCoord(axis)) {
                a = c;
            }
            // compare b to d, lowest could be median candidate
            if (b.getCoord(axis) >= d.getCoord(axis)) {
                b = d;
            }

            // Randomly select between a and b, the innner pair of numbers
            Random R = new Random();
            if (R.nextBoolean()) {
                return a;
            } else {
                return b;
            }
        }
        /*****************************************************************/
        // handle base case where we get 5 elements
        if (nodes.size() == 5) {

            // finds median in only 6 comparisons!!!

            KdNode a, b, c, d, temp; // where a < b and c < d

            // construct ordered pair ab
            if (nodes.get(0).getCoord(axis) < nodes.get(1).getCoord(axis)) {
                a = nodes.get(0);
                b = nodes.get(1);
            } else {
                b = nodes.get(0);
                a = nodes.get(1);
            }

            // construct ordered pair cd
            if (nodes.get(2).getCoord(axis) < nodes.get(3).getCoord(axis)) {
                c = nodes.get(2);
                d = nodes.get(3);
            } else {
                d = nodes.get(2);
                c = nodes.get(3);
            }

            // now compare the lowest two a and c
            // elminate the lowest and reorder that pair as neccessary
            if (a.getCoord(axis) < c.getCoord(axis)) {
                a = nodes.get(4);
                // reorder
                if (a.getCoord(axis) > b.getCoord(axis)) {
                    temp = b;
                    b = a;
                    a = temp;
                }
            } else {
                c = nodes.get(4);
                // reorder
                if (c.getCoord(axis) > d.getCoord(axis)) {
                    temp = d;
                    d = c;
                    c = temp;
                }
            }

            // compare the lowest of the numbers, a and c again, elminate the lower one
            if (a.getCoord(axis) < c.getCoord(axis)) {
                // now compare the one by itself and the lower of the last pair, the lower one is the median
                if (b.getCoord(axis) < c.getCoord(axis)) {
                    return b;
                } else {
                    return c;
                }
            } else {
                if (d.getCoord(axis) < a.getCoord(axis)) {
                    return d;
                } else {
                    return a;
                }
            }
            /************************************************************************/
        } else {
            // break up into sublists of 5 and get the medians of each, then go again
            // additional functionality to not throw away parts of the list that aren't evenly divisible by 5
            // may cause those parts to be over represented
            ArrayList<KdNode> medians = new ArrayList<KdNode>();
            for (int i = 0; i < nodes.size(); i += 5) {
                // get a list of the medians of the medians
                if ((i + 5) <= nodes.size()) {
                    medians.add(SelectMedian(nodes.subList(i, i + 5), axis));
                } else {
                    medians.add(SelectMedian(nodes.subList(i, nodes.size()), axis));
                }
            }
            // now take the median of the list of medians
            return SelectMedian(medians, axis);
        }
    }

    public int getDimension() {
        return dimension;
    }
}
