package Utility;
import java.util.*;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author jrpeterson
 * 
 * special point for use with the KdTree
 * contains all the apparatus for the construction of the KdTree
 */
public class KdNode {
    
    //private int dimension; // the dimension of the space that the point lives in
    
    private int depth; // depth of point within a tree
    
    private double[] coordinates; // the coordinates of this point
    
    private int data; // additional data associated with the point, not used in sorting at all
    private HashMap<String, Object> node_data;

    private KdNode leftchild; // children of the node
    private KdNode rightchild;
    
    public KdNode(double[] coordinates) {
        this.coordinates = coordinates;
        depth = 0;
	node_data = new HashMap<String, Object>();
    }
    
    public KdNode(double[] coordinates,int data) {
        this.coordinates = coordinates;
        depth = 0;
        
        this.data = data;
	node_data = new HashMap<String, Object>();
    }
    
    public void setLeftChild(KdNode child) {
        this.leftchild = child;
    }
    
    public int getData() {
        return data;
    }

    public void setNodeData(String key, Object data){
	node_data.put(key, data);
    }

    public Object getNodeData(String key){
	return node_data.get(key);
    }

    public KdNode getLeftChild() {
        return leftchild;
    }
    
    public void setRightChild(KdNode child) {
        this.rightchild = child;
    }
    
    public KdNode getRightChild() {
        return rightchild;
    }
    
    public void setDepth(int d) {
        this.depth = d;
    }
    
    public int getDepth() {
        return depth;
    }
    
    public double[] getCoord() {
        return coordinates;
    }
    
    public double getCoord(int axis) {
        return coordinates[axis];
    }
    
    public boolean isLeaf() {
        return (leftchild == null) || rightchild == null;

    }
    
    public int getDimension() {
        return coordinates.length;
    }
    
    // computes the euclidean distance between this node and the other node
    public double distance(KdNode target) {
        assert coordinates.length == target.coordinates.length : "Warning incompatible dimensions";
        return Math.sqrt(distance2(target));
    }
    
    public double distance(double[] target) {
        assert coordinates.length == target.length : "Warning incompatible dimensions";
        return Math.sqrt(distance2(target));
    }
    
    // computes the squared distance to the other node
    public double distance2(KdNode target) {
        assert coordinates.length == target.coordinates.length : "Warning incompatible dimensions";
        double d = 0;
        for (int i = 0; i < coordinates.length; i++) {
            d += (coordinates[i] - target.coordinates[i])*(coordinates[i] - target.coordinates[i]);
        }
        return d;
    }
    
    public double distance2(double[] target) {
        assert coordinates.length == target.length : "Warning incompatible dimensions";
        double d = 0;
        for (int i = 0; i < coordinates.length; i++) {
            d += (coordinates[i] - target[i])*(coordinates[i] - target[i]);
        }
        return d;
    }
    
    public void print() {
        String out = "Coordinates: [";
        for (int i = 0; i < coordinates.length; i++) {
            out += Double.toString(coordinates[i]);
            if (i < coordinates.length - 1) {
                out += " , ";
            }
        }
        out += "]";
        System.out.println(out);
    }
    
    
}
