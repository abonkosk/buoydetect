/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Utility;

import java.util.ArrayList;

/**
 *
 * @author jrpeterson
 * algorithms courtesy of "Efficient Non-Maximum Suppresion" Neubeck and Gool
 */
public class Suppression {
    
    // non maximum suppression in 1D, returns arraylist of the maxima
    // uses neighborhood of 3, only directly compared to neighbors
    // won't find maxima on the edge of the vector
    // this is a straight forwards implementation O(n)
    public static ArrayList<Integer> nonMaxima(double[] I) {
        ArrayList<Integer> M = new ArrayList<Integer>();
    
        int i = 1;
        
        while (i+1 < I.length) {
            if (I[i] > I[i+1]) { // if greater than the right value
                if (I[i] >= I[i-1]) { // and greater than the left value
                    M.add(i);
                }
            } else { // we know we are less than the right value
                i++; // so skip to the next value
                while ((i+1 < I.length) && (I[i] <= I[i+1])) { // and keep going until we reach a maxima
                    i++;
                }
                if (i+1 < I.length) { // we have reached a maxima
                    M.add(i); 
                }
            }
            i+= 2; // if we got this far then we are heading downhill, so just skip ahead, since right value can't be local max
        }
        return M;
    }
    
    // adapted for non minimum supression in 1D
    public static ArrayList<Integer> nonMinima(double[] I) {
        ArrayList<Integer> M = new ArrayList<Integer>();
    
        int i = 1;
        
        while (i+1 < I.length) {
            if (I[i] < I[i+1]) { // if less than the right value
                if (I[i] <= I[i-1]) { // and less than the left value
                    M.add(i);
                }
            } else { // we know we are greater than the right value
                i++; // so skip to the next value
                while ((i+1 < I.length) && (I[i] >= I[i+1])) { // and keep going until we reach a minima
                    i++;
                }
                if (i+1 < I.length) { // we have reached a minima
                    M.add(i); 
                }
            }
            i+= 2; // if we got this far then we are heading uphill, so just skip ahead, since right value can't be local min
        }
        return M;
    }
    
    // uses the 2D (n+1) x (n+1) Block NMS described in the paper
    // where n is the neigborhood size 1 is the smallest sensible value
    // returns in the form [row, column] of the max it found
    // paper claims worst case complexity is 4 - 4/(n+1), average complexity < 2.39
    // worst case of the straight forwards is n^2, with average complexity per pixel of 3.08
    // configured to not fire off of plateaus where multiple continous elements are tied at the same value
    public static ArrayList<int[]> nonMaxima(double[][] I, int n) {
        ArrayList<int[]> M = new ArrayList<int[]>(); // the actual maxima
        ArrayList<int[]> C = new ArrayList<int[]>(); // candidates to be maxima from the blocks
        
        int mi,mj; // index of a potential max
        double m; // value of a potential max
        boolean isLmax; // is it really a local maximum
        for (int i = n; i < I.length-n-1; i+=(n+1)) { // local maxima must be atleast n+1 appart, so look at blocks, get max
            for (int j = n; j < I[0].length-n-1; j+=(n+1)) {
                
                mi = i; // initialize to upper left hand corner of block
                mj = j;
                for (int x = 0; x <= n; x++) { // for all the points in the box
                    for (int y = 0; y <= n; y++) {
                        if (I[i+x][j+y] > I[mi][mj]) { // get the maximum value in the box
                            mi = i+x;
                            mj = j+y;
                        }
                    }
                }
                // now compare all elements adjacent to this maximum of the box
                //System.out.println("Box Max at: [" + mi + " , " + mj + "]");
                m = I[mi][mj];
                isLmax = true;
                outerloop: // label for breaking easily
                for (int x = -n; x <= n; x++) {
                    for (int y = -n; y <= n; y++) {
                        // add extra statement to prevent the point triggering itself
                        if ((I[mi+x][mj+y] >= m) && ((x != 0) || (y != 0))) { // if an adjacent element is smaller or equal
                            isLmax = false; // then it isn't a local maxima
                            break outerloop;
                        }
                    }
                }
                // if it is a local max, add it to our list
                if (isLmax) {
                    M.add(new int[] {mi, mj});
                }
            }
        }
        return M;
    }

    public static ArrayList<int[]> nonMaxima(double[][] I, int n, double THRESHOLD) {
        ArrayList<int[]> M = new ArrayList<int[]>(); // the actual maxima
        ArrayList<int[]> C = new ArrayList<int[]>(); // candidates to be maxima from the blocks

        int mi,mj; // index of a potential max
        double m; // value of a potential max
        boolean isLmax; // is it really a local maximum
        for (int i = n; i < I.length-n-1; i+=(n+1)) { // local maxima must be atleast n+1 appart, so look at blocks, get max
            for (int j = n; j < I[0].length-n-1; j+=(n+1)) {

                mi = i; // initialize to upper left hand corner of block
                mj = j;
                for (int x = 0; x <= n; x++) { // for all the points in the box
                    for (int y = 0; y <= n; y++) {
                        if (I[i+x][j+y] > I[mi][mj]) { // get the maximum value in the box
                            mi = i+x;
                            mj = j+y;
                        }
                    }
                }
                // now compare all elements adjacent to this maximum of the box
                //System.out.println("Box Max at: [" + mi + " , " + mj + "]");
                m = I[mi][mj];
		// check whether this value is above our threshold
		if (m < THRESHOLD) {
		    continue; // if not skip ahead
		}

                isLmax = true;
                outerloop: // label for breaking easily
                for (int x = -n; x <= n; x++) {
                    for (int y = -n; y <= n; y++) {
                        // add extra statement to prevent the point triggering itself
                        if ((I[mi+x][mj+y] >= m) && ((x != 0) || (y != 0))) { // if an adjacent element is smaller or equal
                            isLmax = false; // then it isn't a local maxima
                            break outerloop;
                        }
                    }
                }
                // if it is a local max, add it to our list
                if (isLmax) {
                    M.add(new int[] {mi, mj});
                }
            }
        }

        return M;
    }

    // adapted for non-minima suppression in 2D
    public static ArrayList<int[]> nonMinima(double[][] I, int n) {
        ArrayList<int[]> M = new ArrayList<int[]>(); // the actual maxima
        ArrayList<int[]> C = new ArrayList<int[]>(); // candidates to be maxima from the blocks
        
        int mi,mj; // index of a potential min
        double m; // value of a potential min
        boolean isLmin; // is it really a local minimum
        for (int i = n; i < I.length-n-1; i+=(n+1)) { // local minima must be atleast n+1 appart, so look at blocks, get min
            for (int j = n; j < I[0].length-n-1; j+=(n+1)) {
                
                mi = i; // initialize to upper left hand corner of block
                mj = j;
                for (int x = 0; x <= n; x++) { // for all the points in the box
                    for (int y = 0; y <= n; y++) {
                        if (I[i+x][j+y] < I[mi][mj]) { // get the minimum value in the box
                            mi = i+x;
                            mj = j+y;
                        }
                    }
                }
                // now compare all elements adjacent to this maximum of the box
                m = I[mi][mj];
                isLmin = true;
                outerloop: // label for breaking easily
                for (int x = -n; x <= n; x++) {
                    for (int y = -n; y <= n; y++) {
                        // add extra statement to prevent the point triggering itself
                        if ((I[mi+x][mj+y] <= m) && ((x != 0) || (y != 0))) { // if an adjacent element is smaller or equal
                            isLmin = false; // then it isn't a local minima
                            break outerloop;
                        }
                    }
                }
                // if it is a local min, add it to our list
                if (isLmin) {
                    M.add(new int[] {mi, mj});
                }
            }
        }
        
        return M;
    }

    // adapted for non-minima suppression in 2D
    public static ArrayList<int[]> nonMinima(double[][] I, int n, double Threshold) {
        ArrayList<int[]> M = new ArrayList<int[]>(); // the actual maxima
        ArrayList<int[]> C = new ArrayList<int[]>(); // candidates to be maxima from the blocks

        int mi,mj; // index of a potential min
        double m; // value of a potential min
        boolean isLmin; // is it really a local minimum
        for (int i = n; i < I.length-n-1; i+=(n+1)) { // local minima must be atleast n+1 appart, so look at blocks, get min
            for (int j = n; j < I[0].length-n-1; j+=(n+1)) {

                mi = i; // initialize to upper left hand corner of block
                mj = j;
                for (int x = 0; x <= n; x++) { // for all the points in the box
                    for (int y = 0; y <= n; y++) {
                        if (I[i+x][j+y] < I[mi][mj]) { // get the minimum value in the box
                            mi = i+x;
                            mj = j+y;
                        }
                    }
                }
                // now compare all elements adjacent to this maximum of the box
                m = I[mi][mj];
		// if error too high skip it
		if (m > Threshold) {
		    continue; // if not skip ahead
		}
                isLmin = true;
                outerloop: // label for breaking easily
                for (int x = -n; x <= n; x++) {
                    for (int y = -n; y <= n; y++) {
                        // add extra statement to prevent the point triggering itself
                        if ((I[mi+x][mj+y] <= m) && ((x != 0) || (y != 0))) { // if an adjacent element is smaller or equal
                            isLmin = false; // then it isn't a local minima
                            break outerloop;
                        }
                    }
                }
                // if it is a local min, add it to our list
                if (isLmin) {
                    M.add(new int[] {mi, mj});
                }
            }
        }

        return M;
    }
    
    
    // super naive and slow nonMaxima supression in 3D compares each element to all 26 neighbors
    // good luck!
    ArrayList<int[]> nonMaxima(double[][][] I, double Threshold) {
        ArrayList<int[]> maxima = new ArrayList<int[]>();
        
        double value;
        boolean isMax;
        for (int i = 1; i < I.length - 1; i++) {
            for (int j = 1; j < I[0].length - 1; j++) {
                for (int k = 1; k < I[0][0].length -1; k++) {
                    value = I[i][j][k];
                    if (value < Threshold) {
                        continue;
                    } else {
                        isMax = true;
                        outerloop:
                        for (int x = -1; x <= +1; x++) {
                            for (int y = -1; y <= +1; y++) {
                                for (int z = -1; z <= +1; z++) {
                                    if ((I[i+x][j+y][k+z] >= value) && ((x != 0) || (y != 0) || (z != 0))) {
                                        isMax = false;
                                        break outerloop;
                                    }
                                }
                            }
                        }
                        if (isMax) {
                            maxima.add(new int[] {i,j,k});
                        }
                    }
                }
            }
        }
        return maxima;
    }
    
    // checks to confirm that points are not adjacent
    public static boolean consistencyCheck(ArrayList<int[]> points, int n) {
        
        for (int[] p : points) {
            for (int[] otherP: points) {
                // make sure it isn't the same point
                if (otherP != p) {
                    boolean areAdjacent = true;
                    for (int i = 0; i < p.length; i++) {
                        if (Math.abs(p[i] - otherP[i]) > n) {
                            areAdjacent = false;
                            break;
                        }
                    }
                    if (areAdjacent) {
                        System.out.println("p = " + p[0] + " " + p[1] + " adjacent to " + otherP[0] + " " + otherP[1]);
                        return false;
                    }
                }
            }
        }
        return true;
                
    }
}
