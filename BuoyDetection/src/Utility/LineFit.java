/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Utility;

import detect.Constants;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author jrpeterson
 */

/*
 * going to use RANSAC on the corners in a loop to pick out lines, these lines are the objects we care about
 * if the number of points in the line is greater than some number, then its a long one (or 2 small ones end
 * to end should still work right) and less than some number its a short one, and have a lower threshold bellow
 * which we stop finding lines, since there isn't anythign good in the scene
 *
 * need to prefilter the corners so that we don't see all the corners on the arm, perhaps just check all of them
 * and remove the ones within some region where we know the arm is
 */
public class LineFit {

    private ArrayList<Line> Lines;
    private Random R;

    public LineFit(ArrayList<double[]> P) {

        // initialize
        Lines = new ArrayList<Line>();
        R = new Random(23848489);

        int linesize = Integer.MAX_VALUE;
        Line candidate;

        // Do Ransac multiple times until the lines it spits out are too short, while they don't have enough points in them
        // this will be a magic number bassed on the number of corners on the shortest piece we want to detect
        while ((linesize >= Constants.RANSACMINLINESIZE) && (P.size() >= Constants.RANSACMINLINESIZE) && (P.size() >= 2)) {
            // RANSAC
            candidate = RANSAC(P);

            // compute a consensus score and an inlier set
            ArrayList<double[]> candidateP = candidate.getPoints();
            linesize = candidateP.size();
            //System.out.println("Line size: " + linesize);
            // if the line has enough points then add the candidate to our list and remove the points
            if (linesize >= Constants.RANSACMINLINESIZE) {
                // got a good line
                Lines.add(candidate);
                // remove these points from P, so that we don't get them on the next RANSAC run
                for (double[] p : candidateP) {
                    P.remove(p);
                }
            }
            //System.out.println("Points left: " + P.size());
        }

    }

    public static Line RANSAC(ArrayList<double[]> P) {
        int cntr = 0;

        Line curL;
        Line bestL = null;
        ArrayList<double[]> curInliers;
        ArrayList<double[]> bestInliers = null;
        while (cntr < Constants.RANSACITTERATIONS) {

            // grab a random line
            curL = randomLine(P);

            // compute an inlier set
            curInliers = inlierSet(P, curL);

            // on first itteration just keep it
            if ((bestL != null) && (bestInliers != null)) {
                // if we got more inliers
                if (curInliers.size() > bestInliers.size()) {
                    bestInliers = curInliers;
                    bestL = curL;
                }
            } else {
                bestL = curL;
                bestInliers = curInliers;
            }

            cntr++;
        }

        // after that many itterations, we are sure to have the best line with the largest inlier set
        // just merge in inliers with the best line and return it
        bestL.add(bestInliers);
        return bestL;
    }

    // grab a random line
    private static Line randomLine(ArrayList<double[]> P) {
        assert (P.size() > 1) : "Warning need more than a single point to generate a line";
        Random Rtemp = new Random();
        double[] p1 = P.get(Rtemp.nextInt(P.size()));

        double[] p2;
        // now need to grab the second point, but also need to make sure it isn't the same one
        do {
            p2 = P.get(Rtemp.nextInt(P.size()));
        } while (Matrix.equals(p1, p2));

        return new Line(p1,p2);
    }

    // compute the inlier set for this line
    private static ArrayList<double[]> inlierSet(ArrayList<double[]> P, Line L) {

        ArrayList<double[]> inliers = new ArrayList<double[]>();

        ArrayList<double[]> endpoints = L.getPoints();

        double[] C = L.getCentroid();

        // for each point
        double Pdist; //,Cdist; // perpendicular distance to the line and distance from the point to the centroid
        for (double[] p : P) {
            Pdist = L.distance(p);
            // if close enough to the line
            if (Pdist < Constants.RANSACPTHRESHOLD) {
                // check to make sure this isn't an endpoint that we already have
                boolean flag = false;
                for (double[] e : endpoints) {
                    if (p == e) {
                        flag = true;
                    }
                }
                // check to make sure this point isn't too far from the centroid
                // so that we don't canabalize points from other lines
                /*Cdist = L.distanceP(p);
                if (Cdist > Constants.RANSACCTHRESHOLD) {
                    flag = true;
                }*/
                // if not already in the line, then it is an inlier
                if (!flag) {
                    inliers.add(p);
                }
            }
        }

        return inliers;
    }

    public ArrayList<Line> getLines() {
        return Lines;
    }
}
