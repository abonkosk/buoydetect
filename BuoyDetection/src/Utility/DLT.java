/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Utility;

import java.util.ArrayList;

import april.jmat.SingularValueDecomposition;
import april.jmat.LinAlg;
import april.jmat.Matrix;

/**
 *
 * @author jrpeterson
 */

/* the direct linear transform is a way of computing the homography between atleast 4 correpsoning points
 * This solves the equation xprime_i = H*x_i  by attempting to minimize the cross product
 *
 * xprime_i x H*x_i = 0
 *
 * which is transformed into a homogeneous system that is solved using svd
 *
 */
public class DLT {

    private double[][] H; // the computed homography
    private double[] Xcentroid; // the centroid of the x coordinates
    private double[] Xprimecentroid; // centroid of the x' coordinates

    // use doubles since methods may give us more than pixel accuracy... maybe
    public DLT(ArrayList<double[]> X, ArrayList<double[]> Xprime) {
        assert X.size() == Xprime.size() : "Warning points must be given in pairs";
        assert X.size() >= 4 : "Warning need atleast 4 point correpsondances to compute the homography:";

        // normalize the given points just to be sure
        normalize(X);
        normalize(Xprime);

	Utility.Matrix.print(X.get(0));

        // transform points to respective coordinate systems so that the
        // computation of the Homography is more stable
        center(X,Xprime);
	
	Utility.Matrix.print(X.get(0));

        // compute the matrix such that A*h = 0
        double[][] A = composeA(X,Xprime);

        // svd of A
        SingularValueDecomposition svd = new SingularValueDecomposition(new Matrix(A));


        // I have caused a name space collision with april's matrix class need to be a bit careful
        Matrix V = svd.getV();
        // the last column of V is associated with the smallest singular value, that is to say
        // the closest thing to a solution of A*h = 0; this column is the closest to the a right null vector

        // get h as the last column of V
        double[] h = (V.getColumn(V.getColumnDimension()-1)).copyArray(); // need to be careful here, need to convert from mMatrix to Vec to double[]

	Utility.Matrix.print(h);

        this.H = htoH(h); // convert the column vector to a matrix
    }

    // transforms a point from the coordinates in x to xprime
    public double[] transform(double[] x) {
        assert (x.length == 2) || (x.length == 3) : "Warning expected a vector in 2D or homogeneous 2D";
        
        if (x.length == 2) { // need to put into homogeneous coordinates or else
            double[] temp = x;
            x = new double[3];
            x[0] = temp[0];
            x[1] = temp[1];
            x[2] = 1;
        }
	
        // can't forget this step since the homography is between those coordinate systems,
        //not the original ones
        x = centerX(x);

        double[] xprime = LinAlg.matrixAB(H,x); // this step might not work, need to becareful and make sure LinAlg can handle the multiplication
        // of a double[][] by double[]

        // normalize before returning
        xprime = normalize(xprime);

        // get back to the original coordinate system
        xprime = uncenterXprime(xprime);

        return xprime;
    }

    // ensures that homogeneous coordinates have been properly normalized
    private void normalize(ArrayList<double[]> X) {
        if (!X.isEmpty()) {
	    for (int i = 0; i < X.size(); i++) {
                 X.set(i,normalize(X.get(i)));
            }
        }
    }

    private double[] normalize(double[] x) {
        if ((x.length == 3) && (x[2] != 1) && (x[2] != 0)) {
                    x[0] = x[0]/x[2];
                    x[1] = x[1]/x[2];
                    x[2] = 1;
        } else if (x.length == 2) {
	    double[] temp = new double[3];
	    temp[0] = x[0];
	    temp[1] = x[1];
	    temp[2] = 1;
	    x = temp;
	}
	return x;
    }

    /* finds the centroid of the given points in each image and uses it to transforme the coordinate system
     * to make the computation of the Homography more robust
     * points better be normalized or else!
     *
     * since the cross product is saying effectively that we want xprime and H*x to be in the same direction
     * normal image coordinates lead to generally small angles, since the are from the upper left hand corner of the image
     * by shifting the origin of the coordinate system to the centroid of the points in their respective images, we can have much
     * larger angles which should make the computation more numerically stable
     */

    // TODO does this actually do what I think it does, if so would we expect to have seen anything elsewhere?
    private void center(ArrayList<double[]> X, ArrayList<double[]> Xprime) {
        Xcentroid = new double[3];
        Xprimecentroid = new double[3];

        // compute the Xcentroid
        for (double[] x : X) {
            Xcentroid = Utility.Matrix.add(Xcentroid,x);
        }
        Xcentroid = Utility.Matrix.times(1.0/X.size(),Xcentroid);

        // compute the Xprimecentroid
        for (double[] xprime : Xprime) {
            Xprimecentroid = Utility.Matrix.add(Xprimecentroid,xprime);
        }
        Xprimecentroid = Utility.Matrix.times(1.0/Xprime.size(),Xprimecentroid);

        // now that have both centroids translate the points into this new coordinate system
        for (int i = 0; i < X.size(); i++) {
	    double[] x = X.get(i);
	    x = Utility.Matrix.subtract(x,Xcentroid);
            x[2] = 1; // don't want to mess up the homogenous coordinate
	    X.set(i,x);

        }
        for (int i = 0; i < Xprime.size(); i++) {
            double[] xprime = Xprime.get(i);
	    xprime = Utility.Matrix.subtract(xprime,Xprimecentroid);
            xprime[2] = 1; // don't want to mess up the homogenous coordinate
	    Xprime.set(i,xprime);
        }
    }

    // uses the Xcentroid to transform the point x
    private double[] centerX(double[] x) {
        x = Utility.Matrix.subtract(x,Xcentroid);
        x[2] = 1;
	return x;
    }

    private double[] uncenterXprime(double[] xprime) {
        xprime = Utility.Matrix.add(xprime,Xprimecentroid);
        xprime[2] = 1;
	return xprime;
    }

    /* uses the points x and xprime creates the correspondance matrix which satisfies the following equation
     * xprime_i x H*x_i = 0 generates the following pair of equations for each point
     * note that although 3 equations are generated per correspondance, only 2 are linearly independent
     *
     * (i)   -z'*x*h21 - z'*y*h22 - z'*z*h23 + y'*x*h31 + y'*y*h32 + y'*z*h33 = 0
     *
     * (j)    z'*x*h11 + z'*y*h12 + z'*z*h13 - x'*x*h31 - x'*y*h32 - x'*z*h33 = 0
     *
     * the above equations can be represented by a matrix A, such that A*h = 0
     * where h is a column vector of the elements of H [h11, h12, h13, h21, h22, h23, h31, h32, h33]^T
     *
     * note that this function requires 2D homogeneous points
     */
    private double[][] composeA(ArrayList<double[]> X, ArrayList<double[]> Xprime) {
	assert (X.get(0).length == 3) : "Expected Homogeneous Coordinates for X";
	assert (Xprime.get(0).length == 3) : "Expected Homogeneous Coordinates for Xprime";

        double[][] A = new double[2*X.size()][9]; // get 2 equations per point correspondance

        // for each point correspondance create pair of equations
        for (int i = 0; i < X.size(); i++) {

            double x = X.get(i)[0];
            double y = X.get(i)[1];
            double z = X.get(i)[2];
            double xprime = Xprime.get(i)[0];
            double yprime = Xprime.get(i)[1];
            double zprime = Xprime.get(i)[2];

            // i equation
            A[2*i][3] = -zprime*x;      // h21 term
            A[2*i][4] = -zprime*y;      // h22 term
            A[2*i][5] = -zprime*z;      // h23 term
            A[2*i][6] = yprime*x;       // h31 term
            A[2*i][7] = yprime*y;       // h32 term
            A[2*i][8] = yprime*z;       // h33 term

            // second equation
            A[2*i + 1][0] = zprime*x;   // h11 term
            A[2*i + 1][1] = zprime*y;   // h12 term
            A[2*i + 1][3] = zprime*z;   // h13 term
            A[2*i + 1][6] = -xprime*x;  // h31 term
            A[2*i + 1][7] = -xprime*y;  // h32 term
            A[2*i + 1][8] = -xprime*z;  // h33 term
        }
        return A;
    }

    // converts the column vector h into the 3x3 homography matrix
    private double[][] htoH(double[] h) {

        double[][] H = new double[3][3];
        for (int i = 0; i < H.length; i++) {
            for (int j = 0; j < H[0].length; j++) {
                H[i][j] = h[i*H[0].length + j]/h[8];
            }
        }
	Utility.Matrix.print(H);
        return H;
    }
}