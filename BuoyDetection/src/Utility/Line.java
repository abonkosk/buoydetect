/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Utility;

import java.util.ArrayList;

/**
 *
 * @author jrpeterson
 */

/*
 * Line is parameterized as X = Centroid + orientation*t
 */
public class Line {

    private ArrayList<double[]> points; // points that are part of the line
    private double[] centroid; // centroid of the points, tells us where the middle of the line is
    private double[][] Sigma; // covariance of the points in the line = E[(x-E(x))*(x-E(x))^T] = E[x*x^T] - E[x]*E[x]^T
    private double[][] Sigma_sub; // sub term for computing sigma E[x*x^T]
    private double[] orientation; // tells us how the line is orriented in the real world this is the major axis
    private double[] dimensions; // gives us an idea of how long in the major and minor axis

    // initialize line as between some points
    public Line(ArrayList<double[]> P) {
        assert (P.size() >= 2) : "Warning: need atleast 2 points to construct a line";
        assert (P.get(0).length == 2) : "Expected inhomogeneous coordinates!";

        // initialization
        centroid = new double[2];
        Sigma_sub = new double[2][2];
        points = new ArrayList<double[]>();


        for (double[] p : P) {
            accumulate(p);
        }
    
        wrapUp();
    }
    
    public Line(double[] P1, double[] P2) {
        assert (P2.length == 2) : "Expected inhomogeneous coordinates!";
        assert (P1.length == 2) : "Expected inhomogeneous coordinates!";
                
        // initialization
        centroid = new double[2];
        Sigma_sub = new double[2][2];
        points = new ArrayList<double[]>();
        
        accumulate(P1);
        accumulate(P2);
        
        wrapUp();
    }

    // modifies internal values to allow new points to be accumulated
    private void unnormalize() {
        int size = points.size();
        // unnormalize old figures
        centroid = Matrix.times(size, centroid);
        Sigma_sub = Matrix.times(size, Sigma_sub);
    }

    // add new point into internal values
    private void accumulate(double[] p) {
        // accumulate for centroid computation
        centroid = Matrix.add(centroid, p);

        // accumulate sub term for sigma compuation
        Sigma_sub = Matrix.add(Sigma_sub, Matrix.times(Matrix.toColumnVec(p), Matrix.toRowVec(p)));

        // add point to internal arrayList
        points.add(p);
    }
    
    private void wrapUp() {
        // compute expected values
        int size = points.size();
        centroid = Matrix.times(1.0 / size, centroid);
        Sigma_sub = Matrix.times(1.0 / size, Sigma_sub);
        
        // compute sigma
        Sigma = Matrix.subtract(Sigma_sub, Matrix.times(Matrix.toColumnVec(centroid), Matrix.toRowVec(centroid)));
        
        // use the dominant eigen vector of Sigma
        double[][][] ans = Matrix.EigDecomp(Sigma);
        double[][] Lambda = ans[0]; // matrix of eigenvalues
        double[][] Q = ans[1]; // matrix of eigenvectors

        dimensions = new double[]{Lambda[0][0], Lambda[1][1]};
        orientation = Matrix.getColumn(Q, 0); // get eigenvector associated with the largest eigenvalue
    }

    // add a point to the already existing line
    public void add(double[] p) {
        assert (p.length == 2) : "Warning only works for 2D inhomogeneous coordinates";
        unnormalize();
        // add our new point in
        accumulate(p);
        
        wrapUp();
    }

    public void add(ArrayList<double[]> P) {
        int size = points.size();
        // unnormalize old figures
        unnormalize();
        for (double[] p : P) {
            assert (p.length == 2) : "Warning only works for 2D inhomogeneous coordinates";
            accumulate(p);
        }
        
        wrapUp();
    }

    // how many of the points are inliers for the constructed line
    // normal distance to the line
    public double distance(double[] p) {
        assert (p.length == 2) : "Warning only works for 2D inhomogeneous coordinates";

        double d = Matrix.L2norm(Matrix.subtract(Matrix.subtract(centroid, p),
                Matrix.times(Matrix.dotProduct(Matrix.subtract(centroid, p), orientation), orientation)));

        return d;
    }

    // parallel distance to point from centroid along the line
    public double distanceP(double[] p) {
	assert (p.length == 2) : "Warning only works for 2D inhomogeneous coordinates";
	double d = Math.abs(Matrix.dotProduct(Matrix.subtract(centroid, p), orientation));
	return d;
    }

    // returns unit vector describing the line
    public double[] getDirection() {
        return orientation;
    }

    // returns angle describing the orientation of the line
    // in radians hopefully
    // made a modification to make our thing work, its evil!
    public double getAngle() {
        return Math.atan2(orientation[1], orientation[0]);
    }

    public ArrayList<double[]> getPoints() {
        return points;
    }

    public double[] getCentroid() {
        return centroid;
    }

    // returns the eigen values associated with the inner distribution
    // the first should be the large one which reflects the length of the line segment
    // the second is sort of like an error term which reflects how noisy the line is
    public double[] getEigenValues() {
        return dimensions;
    }

    public void print() {
        System.out.println("Line centroid at: " + centroid[0] + " , " + centroid[1]);
        System.out.println("Vector: " + orientation[0] + " , " + orientation[1]);
    }
}
