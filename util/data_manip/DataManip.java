import java.util.*;
import java.io.*;
import java.awt.image.*;
import javax.imageio.*;

public class DataManip {
    static int buoy_instance_id = 0;

    public static void printUsage(){
	System.out.println("java DataManip <data_directory1> <data_directory2> ... ");
    }

    public static void main(String[] args){
	if(args.length == 0){
	    printUsage();
	    return;
	}
	
	new DataManip().run(args);
    }

    public void run(String[] args){
	BuoyData bd = new BuoyData(args);
	if(bd.size() == 0){
	    System.err.println("No buoys to work on...");
	    System.exit(-1);
	}
	
	int max_size = bd.getMaxSize();
	String outdir = "data_manip_"+System.currentTimeMillis();
	bd.generateUniformSizing(outdir, max_size);
    }

    public class BuoyData {
	static final String BUOY_METADATA_FILE = "buoy_list.txt";
	ArrayList<Buoy> buoys;

	public BuoyData(String[] data_dirs){
	    buoys = new ArrayList<Buoy>();
	    for(String dir : data_dirs){
		BufferedReader br;
		try {
		    DataInputStream dis = new DataInputStream(new FileInputStream(dir+"/"+BUOY_METADATA_FILE));
		    br = new BufferedReader(new InputStreamReader(dis));
		} catch(Exception e) {
		    System.err.println("Error: Failed to open Buoy metadata file in the '"+dir+"' directory");
		    continue;
		}

		try {
		    while(br.ready()){
			String line = br.readLine();
			Buoy b = new Buoy(dir, line);
			buoys.add(b);
		    }

		} catch(Exception e){
		    System.err.println("Error: Failed to read buoy data in "+dir+"' directory");
		    continue;
		}
	    }

	    System.out.println("Loaded "+buoys.size()+" Buoys!");
	}

	public int size(){
	    return buoys.size();
	}

	public int getMaxSize(){
	    int big = -1;
	    for(Buoy b : buoys){
		if(b.width > big)
		    big = b.width;
		if(b.height > big)
		    big = b.height;
	    }
	    return big;
	}

	public void generateUniformSizing(String outdir, int size){
	    int count = 0;
	    for(Buoy b : buoys){
		if(b.outputAtSize(outdir, size))
		    count++;
	    }
	    System.out.println("Successfully output "+count+" buoys.");
	}

    }

    public class Buoy {
	public int id;
	public int x0;
	public int y0;
	public int width;
	public int height;
	public String dir;
	public String filename;
	public BufferedImage image;

	public Buoy(String dir_, String s){
	    String[] split_s = s.split(",");
	    try {
		id = buoy_instance_id;
		buoy_instance_id++;
		x0 = Integer.parseInt(split_s[0]);
		y0 = Integer.parseInt(split_s[1]);
		width = Integer.parseInt(split_s[2]);
		height = Integer.parseInt(split_s[3]);
		dir = dir_;
		filename = split_s[4];
		image = ImageIO.read(new File(dir+"/"+filename));
	    } catch(Exception ex){
		System.err.println("Error: Could not read buoy meta-data");
	    }
	}

	public double getCenterX(){
	    return (double)x0 + (double)width/2.0;
	}

	public double getCenterY(){
	    return (double)y0 + 3.0*(double)height/2.0;
	}

	public boolean outputAtSize(String outdir, int size){
	    double cx = getCenterX();
	    double cy = getCenterY();
	    int lx = (int)Math.round(cx - (double)size / 2.0);
	    int ly = (int)Math.round(cy - (double)size / 2.0);
	    int rx = lx + size;
	    int ry = ly + size;
	    
	    // make sure coordinates are valid
	    if(image == null || 
	       lx < 0 || ly < 0 || 
	       rx > image.getWidth() ||
	       ry > image.getHeight()){
		System.err.println("Error: Invalid region for buoy '"+dir+"/"+filename+"' at ("+cx+", "+cy+")... skipping...");
		return false;
	    }
		
	    // crop the image
	    BufferedImage new_image = new BufferedImage(size, size, image.getType());
	    for(int y = ly; y < ry; ++y){
		for(int x = lx; x < rx; ++x){
		    new_image.setRGB(x - lx, y - ly, image.getRGB(x, y));
		}
	    }

	    // save image
	    try {
		String outname = outdir+"/"+new File(dir).getName()+"_"+id+".png";
		File outfile = new File(outname);
		outfile.mkdirs();
		ImageIO.write(new_image, "png", outfile);
	    } catch(Exception e) {
		System.err.println("Error: Failed to crop and write buoy '"+dir+"/"+filename+"' at ("+cx+", "+cy+")... skipping...");
		return false;
	    }
	    
	    return true;
	}
    }
}

